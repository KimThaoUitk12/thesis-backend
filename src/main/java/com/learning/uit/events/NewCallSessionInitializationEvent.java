package com.learning.uit.events;

import org.springframework.context.ApplicationEvent;

public class NewCallSessionInitializationEvent extends ApplicationEvent {

    private String classId;

    public String getClassId() {
        return classId;
    }

    public NewCallSessionInitializationEvent(Object source, String classId) {
        super(source);
        this.classId = classId;
    }
}
