//package com.learning.uit.call;
//
//import com.learning.uit.model.User;
//import com.learning.uit.service.SpecificClassService;
//import com.learning.uit.service.UserService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.core.context.SecurityContextHolder;
//import org.springframework.stereotype.Service;
//
//@Service
//public class CallServiceImpl implements CallService{
//
//    @Autowired
//    private SpecificClassService specificClassService;
//
//    @Autowired
//    private UserService userService;
//
//    @Override
//    public void call(String classId) {
//        specificClassService.findById(classId);
//
//        String userId = ((User)SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getId();
//        if(!(userId.equals(userService.findLearnerByClass(classId)) || userId.equals(userService.findTeacherByClass(classId)))){
//            throw new RuntimeException("you're not belong to this class");
//        }
//    }
//}
