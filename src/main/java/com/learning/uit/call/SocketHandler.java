package com.learning.uit.call;

import com.learning.uit.publisher.Publisher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

@Component
public class SocketHandler extends TextWebSocketHandler {
    List<WebSocketSession> sessions = new CopyOnWriteArrayList<>();

    @Autowired
    private Publisher publisher;

    @Override
    public void afterConnectionEstablished(WebSocketSession session) {
        sessions.add(session);
//        publisher.initClassWithId(getClassIdInSession(session));
    }

    private String getClassIdInSession(WebSocketSession session) {
        return session.getUri().toString().substring(30);
    }

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) {
        sessions.remove(session);
    }

    @Override
    public void handleTextMessage(WebSocketSession session, TextMessage message)
            throws IOException {

//        WebSocketSession s1 = null;
//
//        for(WebSocketSession socketSession : sessions){
//            if(socketSession.getUri().equals(session.getUri())){
//                s1 = session;
//            }
//        }
//
//        for(int i = 0; i < sessions.size(); i++){
//            WebSocketSession s = sessions.get(i);
//            if((s.getUri().equals(session.getUri()) && s1.getUri() != null)
//                    ||s1.getUri() == null){
//                if (!session.equals(s)) {
//                    s.sendMessage(message);
//                    System.out.println(s.getUri());
//                }
//            }
//        }
        for (WebSocketSession webSocketSession : sessions) {
            if (!session.equals(webSocketSession)) {
                webSocketSession.sendMessage(message);
                System.out.println("url: "+webSocketSession.getUri());
                System.out.println("socketId: "+webSocketSession.getId());
            }
        }
    }
}
