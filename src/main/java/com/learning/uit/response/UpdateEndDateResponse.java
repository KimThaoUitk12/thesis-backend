package com.learning.uit.response;

import java.time.Instant;

public class UpdateEndDateResponse {
    private boolean returning;
    private Instant endDate;

    public UpdateEndDateResponse(boolean b) {
        this.returning = b;
    }

    public boolean isReturning() {
        return returning;
    }

    public void setReturning(boolean returning) {
        this.returning = returning;
    }

    public Instant getEndDate() {
        return endDate;
    }

    public void setEndDate(Instant endDate) {
        this.endDate = endDate;
    }

    public UpdateEndDateResponse(boolean returning, Instant endDate) {
        this.returning = returning;
        this.endDate = endDate;
    }
}
