package com.learning.uit.response;

import com.learning.uit.model.Course;
import com.learning.uit.model.Feedback;
import com.learning.uit.model.Subject;
import com.learning.uit.model.User;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Collection;

@Data
@NoArgsConstructor
public class CourseResponse {

    private Course course;

    private Subject subject;

    private double rate;

    private User teacher;

    private Collection<User> students;

    private Collection<Feedback> feedbacks;

    public CourseResponse(Course course, Subject subject, double rate, Collection<Feedback> feedbacks, User teacher, Collection<User> students) {
        this.course = course;
        this.subject = subject;
        this.feedbacks = feedbacks;
        this.rate = rate;
        this.teacher = teacher;
        this.students = students;
    }
}
