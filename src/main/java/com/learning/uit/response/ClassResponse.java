package com.learning.uit.response;

import com.learning.uit.model.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Collection;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ClassResponse {
    private SpecificClass specificClass;
    private User teacher;
    private User student;
    private Course course;
    private Subject subject;
    private Collection<Feedback> feedback;
    private Studying studying;
}
