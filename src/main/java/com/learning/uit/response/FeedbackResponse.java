package com.learning.uit.response;

import com.learning.uit.model.Feedback;
import com.learning.uit.model.SpecificClass;
import com.learning.uit.model.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FeedbackResponse {
    private Feedback feedback;
    private User student;
    private SpecificClass specificClass;
}
