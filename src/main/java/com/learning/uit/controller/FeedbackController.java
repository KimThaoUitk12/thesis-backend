package com.learning.uit.controller;

import com.learning.uit.dto.FeedbackDTO;
import com.learning.uit.helper.HelperService;
import com.learning.uit.model.Feedback;
import com.learning.uit.response.FeedbackResponse;
import com.learning.uit.service.FeedbackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/feedback")
@CrossOrigin("*")
public class FeedbackController {
    @Autowired
    private FeedbackService feedbackService;

    @Autowired
    private HelperService helperService;

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public ResponseEntity<Collection<FeedbackResponse>> findAll(){
        return new ResponseEntity<>(feedbackService.findAll().stream()
                .map(feedback -> helperService.feedbackToFeedbackResponse(feedback)).collect(Collectors.toList()), HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<FeedbackResponse> findById(@PathVariable String id){
        return new ResponseEntity<>(helperService.feedbackToFeedbackResponse(feedbackService.findById(id)), HttpStatus.FOUND);
    }

    @RequestMapping(value = "/byuser/{id}", method = RequestMethod.GET)
    public ResponseEntity<Collection<FeedbackResponse>> findByUser(@PathVariable String id){
        return new ResponseEntity<>(feedbackService.findByUser(id).stream()
                .map(feedback -> helperService.feedbackToFeedbackResponse(feedback)).collect(Collectors.toList()), HttpStatus.OK);
    }

    @RequestMapping(value = "/byclass/{id}", method = RequestMethod.GET)
    public ResponseEntity<Collection<FeedbackResponse>> findByClass(@PathVariable String id){
        return new ResponseEntity<>(feedbackService.findByClass(id).stream()
                .map(feedback -> helperService.feedbackToFeedbackResponse(feedback)).collect(Collectors.toList()), HttpStatus.OK);
    }

    @RequestMapping(value = "/bycourse/{id}", method = RequestMethod.GET)
    public ResponseEntity<Collection<FeedbackResponse>> findByCourse(@PathVariable String id){
        return new ResponseEntity<>(feedbackService.findByCourse(id).stream()
                .map(feedback -> helperService.feedbackToFeedbackResponse(feedback)).collect(Collectors.toList()), HttpStatus.OK);
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public ResponseEntity<FeedbackResponse> create(@RequestBody FeedbackDTO feedbackDTO){
        return new ResponseEntity<>(helperService.feedbackToFeedbackResponse(feedbackService.create(feedbackDTO)), HttpStatus.CREATED);
    }

    @RequestMapping(value = "/update/{id}", method = RequestMethod.PUT)
    public ResponseEntity<FeedbackResponse> update(@PathVariable String id, @RequestBody FeedbackDTO feedbackDTO){
        return new ResponseEntity<>(helperService.feedbackToFeedbackResponse(feedbackService.update(id, feedbackDTO)), HttpStatus.OK);
    }

//    @RequestMapping(value = "/create", method = RequestMethod.GET)
//    public ResponseEntity<Feedback> create(FeedbackDTO feedbackDTO){
//
//    }
}
