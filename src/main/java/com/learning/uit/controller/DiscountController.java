package com.learning.uit.controller;

import com.learning.uit.dto.base.DiscountDTO;
import com.learning.uit.model.base.Discount;
import com.learning.uit.service.DiscountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@RequestMapping("/discount")
@CrossOrigin("*")
public class DiscountController {
    @Autowired
    private DiscountService discountService;

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public ResponseEntity<Collection<Discount>> getAll(){
        return new ResponseEntity<>(discountService.findAll(), HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<Discount> findById(@PathVariable String id){
        return new ResponseEntity<>(discountService.findById(id), HttpStatus.FOUND);
    }

    @RequestMapping(value = "/byname/{name}", method = RequestMethod.GET)
    public ResponseEntity<Collection<Discount>> findByName(@PathVariable String name){
        return new ResponseEntity<>(discountService.findByName(name), HttpStatus.OK);
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public ResponseEntity<Discount> create(@RequestBody DiscountDTO discountDTO){
        return new ResponseEntity<>(discountService.create(discountDTO), HttpStatus.CREATED);
    }

    @RequestMapping(value = "/update/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Discount> update(@PathVariable String id, @RequestBody DiscountDTO discountDTO){
        return new ResponseEntity<>(discountService.updateById(id, discountDTO), HttpStatus.OK);
    }

    @RequestMapping(value = "/amountrange/lower={lower}&upper={upper}", method = RequestMethod.GET)
    public ResponseEntity<Collection<Discount>> beetweenOfAmount(@PathVariable long lower, @PathVariable long upper){
        return new ResponseEntity<>(discountService.beetweenOfAmount(lower, upper), HttpStatus.OK);
    }

    @RequestMapping(value = "/percentagerange/lower={lower}&upper={upper}", method = RequestMethod.GET)
    public ResponseEntity<Collection<Discount>> beetweenOfPercentage(@PathVariable double lower, @PathVariable double upper){
        return new ResponseEntity<>(discountService.beetweenOfPercentage(lower, upper), HttpStatus.OK);
    }
}
