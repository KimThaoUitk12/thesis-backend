package com.learning.uit.controller;

import com.learning.uit.dto.ScheduleDTO;
import com.learning.uit.model.Schedule;
import com.learning.uit.service.ScheduleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@RequestMapping("/schedule")
@CrossOrigin("*")
public class ScheduleController {
    @Autowired
    private ScheduleService scheduleService;

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public ResponseEntity<Collection<Schedule>> findAll(){
        return new ResponseEntity<>(scheduleService.findAll(), HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<Schedule> findById(@PathVariable String id){
        return new ResponseEntity<>(scheduleService.findById(id), HttpStatus.FOUND);
    }

    @RequestMapping(value = "/byteacher/{id}", method = RequestMethod.GET)
    public ResponseEntity<Collection<Schedule>> findByTutor(@PathVariable String id){
        return new ResponseEntity<>(scheduleService.findByTutor(id), HttpStatus.OK);
    }

    @RequestMapping(value = "/bystudent/{id}", method = RequestMethod.GET)
    public ResponseEntity<Collection<Schedule>> findByStudent(@PathVariable String id){
        return new ResponseEntity<>(scheduleService.findByStudent(id), HttpStatus.OK);
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public ResponseEntity<Schedule> create(@RequestBody ScheduleDTO scheduleDTO){
        return new ResponseEntity<>(scheduleService.create(scheduleDTO), HttpStatus.CREATED);
    }

    @RequestMapping(value = "/update/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Schedule> update(@PathVariable String id, @RequestBody ScheduleDTO scheduleDTO){
        return new ResponseEntity<>(scheduleService.update(id, scheduleDTO), HttpStatus.OK);
    }

    @RequestMapping(value = "/byclass/{classId}", method = RequestMethod.GET)
    public ResponseEntity<Collection<Schedule>> getByClass(@PathVariable String classId){
        return new ResponseEntity<>(scheduleService.findByClass(classId), HttpStatus.OK);
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    public ResponseEntity deleteById(@PathVariable String id){
        boolean result = scheduleService.deleteById(id);
        if (result){
            return ResponseEntity.ok().body(null);
        }else {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(null);
        }
    }

    @RequestMapping(value = "/createmany", method = RequestMethod.POST)
    public ResponseEntity<Collection<Schedule>> createMany(@RequestBody Collection<ScheduleDTO> scheduleDTOS){
        return new ResponseEntity<>(scheduleService.createMany(scheduleDTOS), HttpStatus.OK);
    }
}
