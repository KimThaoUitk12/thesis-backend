package com.learning.uit.controller;

import com.learning.uit.dto.SubjectDTO;
import com.learning.uit.model.Subject;
import com.learning.uit.service.SubjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@RequestMapping("/subject")
@CrossOrigin("*")
public class SubjectController {
    @Autowired
    private SubjectService subjectService;

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public ResponseEntity<Collection<Subject>> findAll(){
        return new ResponseEntity<>(subjectService.findAll(), HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<Subject> findById(@PathVariable String id){
        return new ResponseEntity<>(subjectService.findById(id), HttpStatus.OK);
    }

    @RequestMapping(value = "/search/{name}", method = RequestMethod.GET)
    public ResponseEntity<Collection<Subject>> findByName(@PathVariable String name){
        return new ResponseEntity<>(subjectService.findByName(name), HttpStatus.OK);
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public ResponseEntity<Subject> create(@RequestBody SubjectDTO subjectDTO){
        return new ResponseEntity<>(subjectService.create(subjectDTO), HttpStatus.OK);
    }

    @RequestMapping(value = "/update/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Subject> updateById(@PathVariable String id,@RequestBody SubjectDTO subjectDTO){
        return new ResponseEntity<>(subjectService.update(id, subjectDTO), HttpStatus.OK);
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    public ResponseEntity deleteById(@PathVariable String id){
        subjectService.deleteById(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).body(null);
    }

    @RequestMapping(value = "/byclass/{classId}", method = RequestMethod.GET)
    public ResponseEntity<Subject> findByClass(@PathVariable String classId){
        return new ResponseEntity<>(subjectService.findByClass(classId), HttpStatus.OK);
    }
}
