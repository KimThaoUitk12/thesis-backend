package com.learning.uit.controller;

import com.learning.uit.dto.Account;
import com.learning.uit.dto.UpdateUserDTO;
import com.learning.uit.dto.UserDTO;
import com.learning.uit.model.User;
import com.learning.uit.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@RequestMapping("/user")
@CrossOrigin("*")
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public ResponseEntity<Collection<User>> findAllUser(){
        return new ResponseEntity<>(userService.findAll(), HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity findUserById(@PathVariable String id){
        try{
            return new ResponseEntity<>(userService.findById(id), HttpStatus.FOUND);
        }catch (RuntimeException e){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        }
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public ResponseEntity<User> createUser(@RequestBody UserDTO user){
        return new ResponseEntity<>(userService.createUser(user), HttpStatus.CREATED);
    }

    @RequestMapping(value = "/updateinfo/{id}", method = RequestMethod.PUT)
    public ResponseEntity updateInfoByUser(@PathVariable String id, @RequestBody UpdateUserDTO updateUserDTO){
        try{
            return new ResponseEntity<>(userService.updateByUser(id, updateUserDTO), HttpStatus.OK);
        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(e.getMessage());
        }
    }

    @RequestMapping(value = "/search/{name}", method = RequestMethod.GET)
    public ResponseEntity findByName(@PathVariable String name){
        try{
            return new ResponseEntity<>(userService.findByName(name), HttpStatus.OK);
        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(e.getMessage());
        }
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    public ResponseEntity deleteById(@PathVariable String id){
        return new ResponseEntity(userService.deleteById(id), HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "/changepassword/{id}", method = RequestMethod.POST)
    public ResponseEntity changePassword(@PathVariable String id, @RequestBody Account account){
        try {
            userService.updateAccount(id, account);
            return ResponseEntity.ok().body(null);
        }catch (RuntimeException e){
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(e.getMessage());
        }
    }
}
