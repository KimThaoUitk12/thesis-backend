package com.learning.uit.controller;

import com.learning.uit.dto.StudyingDTO;
import com.learning.uit.model.Studying;
import com.learning.uit.service.StudyingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/studying")
@CrossOrigin("*")
public class StudyingController {

    @Autowired
    private StudyingService studyingService;

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public ResponseEntity<Studying> registerLearning(@RequestBody StudyingDTO studyingDTO){
        return new ResponseEntity<>(studyingService.create(studyingDTO), HttpStatus.CREATED);
    }

    @RequestMapping(value = "/update/{studyingId}", method = RequestMethod.PUT)
    public ResponseEntity<Studying> update(@PathVariable String studyingId,
                                           @RequestBody StudyingDTO studyingDTO){
        return new ResponseEntity<>(studyingService.update(studyingId, studyingDTO), HttpStatus.OK);
    }

    //-1 -> not succeed
    // 0 -> studentDel =1 || teacherDel =1
    // 1 -> isDelete = 1
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Integer> delete(@PathVariable String id){
        return new ResponseEntity<>(studyingService.delete(id), HttpStatus.OK);
    }
}
