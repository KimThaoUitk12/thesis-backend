package com.learning.uit.controller;

import com.learning.uit.dto.ClassDTO;
import com.learning.uit.helper.HelperService;
import com.learning.uit.model.SpecificClass;
import com.learning.uit.response.ClassResponse;
import com.learning.uit.service.SpecificClassService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestController
@RequestMapping("/class")
@CrossOrigin("*")
public class ClassController {
    @Autowired
    private SpecificClassService specificClassService;

    @Autowired
    private HelperService helperService;

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public ResponseEntity<Collection<ClassResponse>> findAll(){
        return new ResponseEntity<>(specificClassService.findAll().stream().map(specificClass -> helperService.specificClassToClassResponse(specificClass))
                .collect(Collectors.toList()), HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<ClassResponse> findById(@PathVariable String id){
        return new ResponseEntity<>(helperService.specificClassToClassResponse(specificClassService.findById(id)), HttpStatus.FOUND);
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public ResponseEntity<ClassResponse> create(@RequestBody ClassDTO classDTO){
        return new ResponseEntity<>(helperService.specificClassToClassResponse(specificClassService.create(classDTO)), HttpStatus.CREATED);
    }

    @RequestMapping(value = "/update/{id}", method = RequestMethod.PUT)
    public ResponseEntity<ClassResponse> update(@PathVariable String id, @RequestBody ClassDTO classDTO){
        return new ResponseEntity<>(helperService.specificClassToClassResponse(specificClassService.updateById(id, classDTO)), HttpStatus.OK);
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    public ResponseEntity deleteById(@PathVariable String id){
        return new ResponseEntity(specificClassService.deleteById(id), HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "/byteacher/{teacherId}", method = RequestMethod.GET)
    public ResponseEntity<Collection<ClassResponse>> findByTeacher(@PathVariable String teacherId){
        return new ResponseEntity<>(specificClassService.findByTutor(teacherId).stream().map(specificClass -> helperService.specificClassToClassResponse(specificClass))
                .collect(Collectors.toList()), HttpStatus.OK);
    }

    @RequestMapping(value = "/bystudent/{studentId}", method = RequestMethod.GET)
    public ResponseEntity<Collection<ClassResponse>> findByStudent(@PathVariable String studentId){
        return new ResponseEntity<>(specificClassService.findByStudent(studentId).stream().map(specificClass -> helperService.specificClassToClassResponse(specificClass))
                .collect(Collectors.toList()), HttpStatus.OK);
    }

    @RequestMapping(value = "/allbyuser/{userId}", method = RequestMethod.GET)
    public ResponseEntity<Collection<ClassResponse>> findAllByUser(@PathVariable String userId){
        Collection<ClassResponse> learningClass = specificClassService.findByStudent(userId).stream().map(specificClass -> helperService.specificClassToClassResponse(specificClass))
                .collect(Collectors.toList());

        Collection<ClassResponse> teachingClass = specificClassService.findByTutor(userId).stream().map(specificClass -> helperService.specificClassToClassResponse(specificClass))
                .collect(Collectors.toList());

        Stream<ClassResponse> combination = Stream.concat(
                learningClass.stream(), teachingClass.stream());

        return new ResponseEntity<>(combination.collect(Collectors.toList()), HttpStatus.OK);
    }
}
