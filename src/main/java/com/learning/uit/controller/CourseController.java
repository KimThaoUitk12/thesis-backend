package com.learning.uit.controller;

import com.learning.uit.dto.CourseDTO;
import com.learning.uit.helper.FileStorageService;
import com.learning.uit.helper.HelperService;
import com.learning.uit.model.Course;
import com.learning.uit.response.CourseResponse;
import com.learning.uit.service.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/course")
@CrossOrigin("*")
public class CourseController {

    @Autowired
    private CourseService courseService;

    @Autowired
    private FileStorageService fileStorageService;

    @Autowired
    private HelperService helperService;

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public ResponseEntity<Collection<CourseResponse>> getAll(){
        return new ResponseEntity<>(courseService.findAll().stream().map(course -> helperService.courseToCourseResponse(course)).collect(Collectors.toList()), HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<CourseResponse> findById(@PathVariable String id){
        return new ResponseEntity<>(helperService.courseToCourseResponse(courseService.findById(id)), HttpStatus.OK);
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE
            , consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<CourseResponse> create(@RequestParam("file") MultipartFile file , @RequestParam("data") String strCourse) throws IOException {
        String fileName = fileStorageService.storeFile(file);
        return  new ResponseEntity<>(helperService.courseToCourseResponse(courseService.create(strCourse,fileName)), HttpStatus.CREATED);
    }

    @RequestMapping(value = "/update/{id}", method = RequestMethod.PUT)
    public ResponseEntity<CourseResponse> updateById(@PathVariable String id, @RequestBody CourseDTO courseDTO){
        return new ResponseEntity<>(helperService.courseToCourseResponse(courseService.update(id, courseDTO)), HttpStatus.OK);
    }

    @RequestMapping(value = "/bysubject/{subjectId}", method = RequestMethod.GET)
    public ResponseEntity<Collection<CourseResponse>> findBySubject(@PathVariable String subjectId){
        return new ResponseEntity<>(courseService.findBySubject(subjectId).stream().map(course -> helperService.courseToCourseResponse(course)).collect(Collectors.toList()), HttpStatus.OK);
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    public ResponseEntity deleteById(@PathVariable String id){
        courseService.deleteById(id);
        return  ResponseEntity.status(HttpStatus.NO_CONTENT).body(null);
    }

    @RequestMapping(value = "/byclass/{classId}", method = RequestMethod.GET)
    public ResponseEntity<CourseResponse> findByClass(@PathVariable String classId){
        return new ResponseEntity<>(helperService.courseToCourseResponse(courseService.findByClass(classId)), HttpStatus.OK);
    }

    @RequestMapping(value = "/byuser/{userId}", method = RequestMethod.GET)
    public ResponseEntity<Collection<CourseResponse>> findByUser(@PathVariable String userId){
        Collection<Course> courses = new ArrayList<>();
        for(Course course : courseService.findAll()){
            if(course.getUser() != null){
                if(course.getUser().getId().equals(userId)){
                    courses.add(course);
                }
            }
        }
        return new ResponseEntity(courses.stream().map(course -> helperService.courseToCourseResponse(course)), HttpStatus.OK);
    }
}
