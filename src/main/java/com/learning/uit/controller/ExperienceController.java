package com.learning.uit.controller;

import com.learning.uit.dto.base.ExperienceDTO;
import com.learning.uit.model.base.Experience;
import com.learning.uit.service.ExperienceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@RequestMapping("/experience")
public class ExperienceController {

    @Autowired
    private ExperienceService experienceService;

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public ResponseEntity<Collection<Experience>> getAll(){
        return new ResponseEntity<>(experienceService.findAll(), HttpStatus.OK);
    }

    @RequestMapping(value = "/byuser/{userId}", method = RequestMethod.GET)
    public ResponseEntity<Collection<Experience>> getByUser(@PathVariable String userId){
        return new ResponseEntity<>(experienceService.findByUser(userId), HttpStatus.OK);
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public ResponseEntity<Experience> create(@RequestBody ExperienceDTO experienceDTO){
        return new ResponseEntity<>(experienceService.create(experienceDTO), HttpStatus.CREATED);
    }

    @RequestMapping(value = "/update/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Experience> updateById(@PathVariable String id, @RequestBody ExperienceDTO experienceDTO){
        return new ResponseEntity<>(experienceService.updateById(id, experienceDTO), HttpStatus.OK);
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    public ResponseEntity deleteById(@PathVariable String id){
        return new ResponseEntity(experienceService.deleteById(id), HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "/createmany", method = RequestMethod.POST)
    public ResponseEntity<Collection<Experience>> createMany(@RequestBody Collection<ExperienceDTO> experienceDTOS){
        return new ResponseEntity<>(experienceService.saveMany(experienceDTOS), HttpStatus.OK);
    }
}
