package com.learning.uit.controller;

import com.learning.uit.dto.SessionDTO;
import com.learning.uit.model.Session;
import com.learning.uit.service.SessionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/session")
public class SessionController {
    @Autowired
    private SessionService sessionService;

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public ResponseEntity<Session> create(@RequestBody SessionDTO sessionDTO){
        return new ResponseEntity<>(sessionService.create(sessionDTO), HttpStatus.OK);
    }
}
