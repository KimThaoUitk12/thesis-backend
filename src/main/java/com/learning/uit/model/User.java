package com.learning.uit.model;

//import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.learning.uit.helper.CodeHelper;
import com.learning.uit.model.base.BaseModel;
import com.learning.uit.model.base.Experience;
import lombok.*;
import net.minidev.json.annotate.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

@Entity
@Data
@AllArgsConstructor
//@NoArgsConstructor
@Builder
@Table(name = "user")
//@JsonIdentityInfo(generator= ObjectIdGenerators.PropertyGenerator.class, property="id")
public class User extends BaseModel implements Serializable {

    private String userCode = CodeHelper.generateCode("user");

    private String firstName;

    private String lastName;

    @Column(unique=true)
    private String email;

    private String password;

    private String avatar;

    private String address;

    private String phone;

    private int enabled = 0;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "user_role", joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id"))
    @EqualsAndHashCode.Exclude @ToString.Exclude
    private Set<Role> roles = new HashSet<>();

    @JsonManagedReference
    @OneToMany(mappedBy = "user")
    @EqualsAndHashCode.Exclude @ToString.Exclude
    private Collection<Teaching> teachings;

    @JsonManagedReference
    @OneToMany(mappedBy = "user")
    @EqualsAndHashCode.Exclude @ToString.Exclude
    private Collection<Studying> studyings;

    @JsonManagedReference
    @OneToMany(mappedBy = "user")
    @EqualsAndHashCode.Exclude @ToString.Exclude
    private Collection<Course> courses;

    @JsonManagedReference
    @OneToMany(mappedBy = "user", cascade=CascadeType.ALL, fetch=FetchType.LAZY)
    @EqualsAndHashCode.Exclude @ToString.Exclude
    private Collection<Experience> experiences;

    public User() {
        this.setCreatedBy(this.getId());
    }
}