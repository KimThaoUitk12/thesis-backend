package com.learning.uit.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.learning.uit.helper.CodeHelper;
import com.learning.uit.model.base.BaseModel;
import lombok.*;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "course")
public class Course extends BaseModel {
    private String courseCode = CodeHelper.generateCode("course");

    private String name;

    private long duration;

    private String syllabus;

    private long fee;

    private String introVideo;

    @OneToMany(mappedBy = "course")
    @JsonManagedReference
    @EqualsAndHashCode.Exclude @ToString.Exclude
    private Collection<SpecificClass> specificClasses;

    @JsonBackReference
    @ManyToOne
    @EqualsAndHashCode.Exclude @ToString.Exclude
    @JoinColumn(name = "user_id")
    private User user;



//    @OneToMany(mappedBy = "course")
//    @JsonManagedReference
//    @EqualsAndHashCode.Exclude @ToString.Exclude
//    private Collection<Schedule> schedules;

    @JsonBackReference
    @ManyToOne
    @EqualsAndHashCode.Exclude @ToString.Exclude
    @JoinColumn(name = "subject_id")
    private Subject subject;
}
