package com.learning.uit.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.learning.uit.helper.CodeHelper;
import com.learning.uit.model.base.BaseModel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "feedback")
public class Feedback extends BaseModel {

    private String feedbackCode = CodeHelper.generateCode("feedback");

    private String content;

    private int rate;

    private String description;

    private String createdBy;

    private String updatedBy;

    @OneToOne
    @JsonBackReference
    @JoinColumn(name = "studying_id")
    private Studying studying;
}