package com.learning.uit.model;

import com.learning.uit.model.base.Experience;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("job")
public class JobExperience extends Experience {
}

