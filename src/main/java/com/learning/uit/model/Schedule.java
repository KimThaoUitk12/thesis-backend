package com.learning.uit.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.learning.uit.model.base.BaseModel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.LocalTime;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "schedule")
public class Schedule extends BaseModel {
    private int day;

    private LocalTime startAt;

    private LocalTime endAt;


    @ManyToOne
    @JsonBackReference
    @JoinColumn(name = "class_id")
    private SpecificClass sepecificClass;
}

