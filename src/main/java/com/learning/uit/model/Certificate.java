package com.learning.uit.model;

import com.learning.uit.model.base.Experience;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("certificate")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Certificate extends Experience {
    private int year;
}
