package com.learning.uit.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.learning.uit.model.base.BaseModel;
import com.learning.uit.model.base.Discount;
import lombok.*;

import javax.persistence.*;
import java.util.Collection;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@Table(name = "studying")
public class Studying extends BaseModel {
    private int paid;

    private double refund;

    private int studentDel = 0;

    private int teacherDel = 0;

    @ManyToOne
    @JsonBackReference
    @JoinColumn(name = "user_id")
    @EqualsAndHashCode.Exclude @ToString.Exclude
    private User user;

    @OneToOne
    @JsonBackReference
    @JoinColumn(name = "class_id")
    private SpecificClass specificClass;

    @JsonManagedReference
    @JoinTable(name = "studying_discount", //Tạo ra một join Table tên là "address_person"
            joinColumns = @JoinColumn(name = "studying_id"),  // TRong đó, khóa ngoại chính là address_id trỏ tới class hiện tại (Address)
            inverseJoinColumns = @JoinColumn(name = "discount_id")
    )
    @ElementCollection
    private Collection<Discount> discounts;
}
