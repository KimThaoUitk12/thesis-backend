package com.learning.uit.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.learning.uit.model.base.BaseModel;
import lombok.*;

import javax.persistence.*;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "teaching")
public class Teaching extends BaseModel {
    private int received = 0; // 1 is received, 0 did not receive

    @ManyToOne
    @JsonBackReference
    @EqualsAndHashCode.Exclude @ToString.Exclude
    @JoinColumn(name = "user_id")
    private User user;

    @OneToOne
    @JsonBackReference
    @EqualsAndHashCode.Exclude @ToString.Exclude
    @JoinColumn(name = "specific_class")
    private SpecificClass specificClass;
}
