package com.learning.uit.model.base;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.learning.uit.helper.CodeHelper;
import com.learning.uit.model.Studying;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.Instant;
import java.util.Collection;

@Entity
@Data
@NoArgsConstructor
@Builder
@AllArgsConstructor
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="discount_type",
        discriminatorType = DiscriminatorType.STRING)
@Table(name = "discount")
public class Discount extends BaseModel{
    private String name;

    private String discountCode = CodeHelper.generateCode("discount");

    private Instant validFrom;

    private Instant validTo;

    private int status;

    @ManyToMany(mappedBy = "discounts")
    @JsonManagedReference
    private Collection<Studying> studyings;
}
