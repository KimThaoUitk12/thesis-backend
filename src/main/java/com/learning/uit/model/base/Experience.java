package com.learning.uit.model.base;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.learning.uit.helper.CodeHelper;
import com.learning.uit.model.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="experience_type",
        discriminatorType = DiscriminatorType.STRING)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Experience extends BaseModel{
    private String experienceCode = CodeHelper.generateCode("experience");

    private String name;

    private int confirmed;

    @Transient
    public String getType(){
        DiscriminatorValue val = this.getClass().getAnnotation( DiscriminatorValue.class );

        return val == null ? null : val.value();
    }

    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "user_id" ,nullable = false, updatable = true, insertable = true)
    private User user;
}
