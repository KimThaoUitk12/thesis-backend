package com.learning.uit.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.learning.uit.helper.CodeHelper;
import com.learning.uit.model.base.BaseModel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "subject")
public class Subject extends BaseModel {
    private String subjectCode = CodeHelper.generateCode("subject");

    private String name;

    private String category;

    @JsonManagedReference
    @OneToMany(mappedBy = "subject", cascade= CascadeType.ALL, fetch= FetchType.LAZY)
    private Collection<Course> courses;

}