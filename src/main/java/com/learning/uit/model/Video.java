package com.learning.uit.model;

import com.learning.uit.helper.CodeHelper;
import com.learning.uit.model.base.BaseModel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "video")
public class Video extends BaseModel {

    private String videoCode = CodeHelper.generateCode("video");

    private long duration;

    @ManyToOne
    @JoinColumn(name = "session_id")
    private Session session;
}
