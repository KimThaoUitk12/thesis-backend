package com.learning.uit.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.learning.uit.helper.CodeHelper;
import com.learning.uit.model.base.BaseModel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.Instant;
import java.util.Collection;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "session")
public class Session extends BaseModel {
    private String sessionCode = CodeHelper.generateCode("session");

    private String name;

    private Instant startAt;

    private Instant endAt;

    private long duration;

    @ManyToOne
    @JsonBackReference
    @JoinColumn(name = "class_id")
    private SpecificClass specificClass;

    @JsonManagedReference
    @OneToMany(mappedBy = "session")
    private Collection<Video> videos;

}
