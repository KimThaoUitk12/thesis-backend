package com.learning.uit.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.learning.uit.common.Constants;
import com.learning.uit.helper.CodeHelper;
import com.learning.uit.model.base.BaseModel;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.util.Collection;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "specific_class")
public class SpecificClass extends BaseModel {
    private String classCode = CodeHelper.generateCode("class");

    private String className;

    private Instant begin;

    private String syllabus;

    private Instant end = begin;

    @NotBlank
    private String status = Constants.CLASS_READY;

    @JsonManagedReference
    @OneToMany(mappedBy = "sepecificClass")
    @EqualsAndHashCode.Exclude @ToString.Exclude
    private Collection<Schedule> schedules;

    @ManyToOne
    @JsonBackReference
    @EqualsAndHashCode.Exclude @ToString.Exclude
    @JoinColumn(name = "course_id")
    private Course course;

    @JsonManagedReference
    @OneToMany(mappedBy = "specificClass")
    private Collection<Session> sessions;
}