package com.learning.uit.model;

import com.learning.uit.model.base.Discount;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("percentage")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PercentageDiscount extends Discount {
    private double percentage;
}
