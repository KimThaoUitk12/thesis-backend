package com.learning.uit.model;

import com.learning.uit.model.base.Discount;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("amount")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AmountDiscount extends Discount {
    private long amount;
}
