package com.learning.uit.dto;

import com.learning.uit.dto.base.DiscountDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class AmountDiscountDTO extends DiscountDTO {
    private long amount;
}
