package com.learning.uit.dto;

import com.learning.uit.dto.base.BaseDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FeedbackDTO extends BaseDTO {
    private String content;

    private int rate;

    private String studyingId;
}
