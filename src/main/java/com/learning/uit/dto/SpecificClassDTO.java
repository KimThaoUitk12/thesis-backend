package com.learning.uit.dto;

import com.learning.uit.dto.base.BaseDTO;
import com.learning.uit.model.Course;
import com.learning.uit.model.Schedule;
import com.learning.uit.model.Session;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotBlank;
import java.time.Instant;
import java.util.Collection;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SpecificClassDTO extends BaseDTO {
    private String className;

    private Instant begin;

    private String syllabus;

    private String status; // 0 is closed, 1 is open

    private Collection<ScheduleDTO> scheduleDTOS;

    private Course course;

    private Collection<SessionDTO> sessionDTOS;
}
