package com.learning.uit.dto;

import com.learning.uit.dto.base.BaseDTO;
import com.learning.uit.model.Course;
import com.learning.uit.model.SpecificClass;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.time.LocalTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ScheduleDTO extends BaseDTO {
    private int day;

    private String startAt;

    private String endAt;

    private String sepecificClassId;
}
