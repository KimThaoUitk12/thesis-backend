package com.learning.uit.dto;

import com.learning.uit.dto.base.BaseDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SubjectDTO extends BaseDTO {
    private String name;

    private String category;

}
