package com.learning.uit.dto;

import com.learning.uit.dto.base.BaseDTO;
import com.learning.uit.model.SpecificClass;
import com.learning.uit.model.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TeachingDTO extends BaseDTO {
    private int received;

    private String userId;

    private String specificClassId;
}
