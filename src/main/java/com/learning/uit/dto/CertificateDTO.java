package com.learning.uit.dto;

import com.learning.uit.dto.base.ExperienceDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CertificateDTO extends ExperienceDTO {
    private int year;
}
