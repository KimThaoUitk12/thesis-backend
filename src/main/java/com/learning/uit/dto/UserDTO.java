package com.learning.uit.dto;

import com.learning.uit.dto.base.BaseDTO;
import com.learning.uit.dto.base.ExperienceDTO;
import com.learning.uit.model.Studying;
import com.learning.uit.model.Teaching;
import com.learning.uit.model.base.Experience;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.OneToMany;
import java.util.Collection;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserDTO extends BaseDTO {
    private String firstName;

    private String lastName;

    private String email;

    private String password;

    private String avatar;

    private String address;

    private String phone;

    private int enabled = 0;

    private Collection<ExperienceDTO> experienceDTOS;

    private Set<String> roles;

}
