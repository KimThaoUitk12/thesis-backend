package com.learning.uit.dto;

import com.learning.uit.dto.base.BaseDTO;
import com.learning.uit.model.Feedback;
import com.learning.uit.model.SpecificClass;
import com.learning.uit.model.User;
import com.learning.uit.model.base.Discount;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Collection;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class StudyingDTO extends BaseDTO {
    private int paid;

    private double refund;

    private String userId;

    private String specificClassId;

    private Collection<String> discountIds;
}
