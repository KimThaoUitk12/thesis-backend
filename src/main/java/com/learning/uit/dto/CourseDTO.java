package com.learning.uit.dto;

import com.learning.uit.dto.base.BaseDTO;
import com.learning.uit.model.Schedule;
import com.learning.uit.model.SpecificClass;
import com.learning.uit.model.Subject;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Collection;
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CourseDTO extends BaseDTO {
    private String name;

    private long duration;

    private String syllabus;

    private long fee;

    private String introVideo;

    private Collection<String> classIds;

    private Collection<String> scheduleIds;

    private String subjectId;

    private String userId;
}
