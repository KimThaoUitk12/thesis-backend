package com.learning.uit.dto;

import com.learning.uit.dto.base.BaseDTO;
import com.learning.uit.helper.HelperService;
import com.learning.uit.model.SpecificClass;
import com.learning.uit.model.Video;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Collection;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SessionDTO extends BaseDTO {
    private String name;

    private String startAt;

    private String endAt;

    private long duration = ChronoUnit.MINUTES.between(HelperService.stringToInstant(startAt), HelperService.stringToInstant(endAt)) * 60;

    private SpecificClass specificClass;

//    private Collection<Video> videos;
}
