package com.learning.uit.dto;

import com.learning.uit.dto.base.DiscountDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class PercentageDiscountDTO extends DiscountDTO {
    private double percentage;
}
