package com.learning.uit.dto.base;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BaseDTO {
    private int isDeleted;

    private String description;

    private String createdBy;

    private String updatedBy;
}
