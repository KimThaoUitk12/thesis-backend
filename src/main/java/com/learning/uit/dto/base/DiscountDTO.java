package com.learning.uit.dto.base;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.learning.uit.dto.AmountDiscountDTO;
import com.learning.uit.dto.PercentageDiscountDTO;
import com.learning.uit.model.Studying;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.ManyToMany;
import java.time.Instant;
import java.util.Collection;

@AllArgsConstructor
@NoArgsConstructor
@Data
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonSubTypes({@JsonSubTypes.Type(value = AmountDiscountDTO.class, name = "amount"),
        @JsonSubTypes.Type(value = PercentageDiscountDTO.class, name = "percent")})
public class DiscountDTO extends BaseDTO{
    private String name;

    private String validFrom;

    private String validTo;

    private int status;

    private Collection<String> studyings;
}
