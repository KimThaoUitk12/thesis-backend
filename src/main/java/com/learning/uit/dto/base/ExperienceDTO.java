package com.learning.uit.dto.base;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.learning.uit.dto.*;
import com.learning.uit.model.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonSubTypes({@JsonSubTypes.Type(value = CertificateDTO.class, name = "certificate"),
        @JsonSubTypes.Type(value = JobExperienceDTO.class, name = "job")})
public class ExperienceDTO extends BaseDTO {
    protected String name;

    protected int confirmed;

    protected String userId;
}
