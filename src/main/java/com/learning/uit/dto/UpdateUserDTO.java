package com.learning.uit.dto;

import com.learning.uit.dto.base.BaseDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UpdateUserDTO extends BaseDTO {
    private String firstName;

    private String lastName;

    private String avatar;

    private String address;

    private String phone;
}
