package com.learning.uit.dto;

import com.learning.uit.dto.base.BaseDTO;
import com.learning.uit.model.Course;
import com.learning.uit.model.Schedule;
import com.learning.uit.model.Session;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.time.Instant;
import java.util.Collection;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ClassDTO extends BaseDTO {
    private String className;

    private String begin;

    private String syllabus;

    private String status;

    private String end;

    private Collection<String> scheduleIds;

    private String courseId;

    private Collection<String> sessionIds;

    private TeachingDTO teachingDTO;

    private Collection<ScheduleDTO> scheduleDTOS;

}
