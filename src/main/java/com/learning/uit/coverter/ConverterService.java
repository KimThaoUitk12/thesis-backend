package com.learning.uit.coverter;

import com.learning.uit.common.ERole;
import com.learning.uit.dto.*;
import com.learning.uit.dto.base.DiscountDTO;
import com.learning.uit.dto.base.ExperienceDTO;
import com.learning.uit.helper.HelperService;
import com.learning.uit.model.*;
import com.learning.uit.model.base.Discount;
import com.learning.uit.model.base.Experience;
import com.learning.uit.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.lang.reflect.Field;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
@Service
public class ConverterService {
    @Autowired
    private SubjectService subjectService;

    @Autowired
    private SessionService sessionService;

    @Autowired
    private ScheduleService scheduleService;

    @Autowired
    private SpecificClassService specificClassService;

    @Autowired
    private TeachingService teachingService;

    @Autowired
    private UserService userService;

    @Autowired
    private DiscountService discountService;

    @Autowired
    private CourseService courseService;

    @Autowired
    private StudyingService studyingService;

    @Autowired
    private ConverterService converterService;

    @Autowired
    private HelperService helperService;

    public User userDTOToUser(UserDTO userDTO){
        Collection<Experience> experiences = new ArrayList<>();
        for(ExperienceDTO experienceDTO : userDTO.getExperienceDTOS()){
            experiences.add(converterService.experienceDTOToExperience(experienceDTO));
        }
        Set<Role> roles = new HashSet<>();
        for (String str : userDTO.getRoles()){
            roles.add(converterService.stringToRole(str));
        }
        return User.builder()
                .address(userDTO.getAddress())
                .avatar(userDTO.getAvatar())
                .email(userDTO.getEmail())
                .firstName(userDTO.getFirstName())
                .lastName(userDTO.getLastName())
                .phone(userDTO.getPhone())
                .roles(roles)
                .experiences(experiences)
                .build();
    }

    public Role stringToRole(String strRole){
        return Role.builder().eRole(ERole.valueOf(strRole)).build();
    }

    public Subject subjectDTOToSubject(SubjectDTO subjectDTO){
        return Subject.builder()
                .name(subjectDTO.getName())
                .category(subjectDTO.getCategory())
                .build();
    }

    public Course courseDTOToCourse(CourseDTO courseDTO){
        return Course.builder()
                .syllabus(courseDTO.getSyllabus())
                .fee(courseDTO.getFee())
                .duration(courseDTO.getDuration())
                .name(courseDTO.getName())
                .user(userService.findById(courseDTO.getUserId()))
                .subject(subjectService.findById(courseDTO.getSubjectId()))
                .specificClasses(courseDTO.getClassIds() != null ? specificClassService.findManyByIds(courseDTO.getClassIds()) : null)
                .introVideo(courseDTO.getIntroVideo()).build();
    }

//    public SpecificClass classDTOToClass(SpecificClassDTO specificClassDTO){
//        return SpecificClass.builder()
//                .className(specificClassDTO.getClassName())
//                .begin(specificClassDTO.getBegin())
//                .syllabus(specificClassDTO.getSyllabus())
//                .status(specificClassDTO.getStatus())
//                .course(specificClassDTO.getCourse())
//                .build();
//    }

//    public Feedback FeedbackDTOToFeedback(FeedbackDTO feedbackDTO){
//        return Feedback.builder()
//                .rate(feedbackDTO.getRate())
//                .content(feedbackDTO.getContent())
//                .description(feedbackDTO.getDescription())
//                .build();
//    }

    public SpecificClass classDTOToClass(ClassDTO classDTO){
        return SpecificClass.builder()
                .className(classDTO.getClassName())
                .syllabus(classDTO.getSyllabus())
                .status(classDTO.getStatus())
                .begin(helperService.stringToInstant(classDTO.getBegin()))
                .sessions(classDTO.getSessionIds() != null ? sessionService.findManyByIds(classDTO.getSessionIds()) : null)
                .schedules(classDTO.getScheduleIds() != null ? scheduleService.findManyByIds(classDTO.getScheduleIds()) : null)
                .course(courseService.findById(classDTO.getCourseId()))
                .build();
    }

    public Teaching TeachingDTOToTeaching(TeachingDTO teachingDTO){
        return Teaching.builder()
                .user(userService.findById(teachingDTO.getUserId()))
                .specificClass(specificClassService.findById(teachingDTO.getSpecificClassId()))
                .received(teachingDTO.getReceived())
                .build();
    }

    public Studying StudyingDTOToStudying(StudyingDTO studyingDTO){
        Collection<Discount> discounts = discountService.findMany(studyingDTO.getDiscountIds());
        return Studying.builder()
                .paid(studyingDTO.getPaid())
                .refund(studyingDTO.getRefund())
                .specificClass(specificClassService.findById(studyingDTO.getSpecificClassId()))
                .user(userService.findById(studyingDTO.getUserId()))
                .discounts(discounts)
                .build();
    }

    public Schedule scheduleDTOToSchedule(ScheduleDTO scheduleDTO){
        return Schedule.builder()
                .day(scheduleDTO.getDay())
                .startAt(LocalTime.parse(scheduleDTO.getStartAt()))
                .endAt(LocalTime.parse(scheduleDTO.getEndAt()))
                .sepecificClass(specificClassService.findById(scheduleDTO.getSepecificClassId()))
                .build();
    }

    public Session sessionDTOToSession(SessionDTO sessionDTO){
        return Session.builder()
                .duration(sessionDTO.getDuration())
                .startAt(HelperService.stringToInstant(sessionDTO.getStartAt()))
                .endAt(HelperService.stringToInstant(sessionDTO.getEndAt()))
                .specificClass(sessionDTO.getSpecificClass())
                .build();

    }

    public Experience experienceDTOToExperience(ExperienceDTO experienceDTO){
        Experience experience = null;

        if(experienceDTO instanceof CertificateDTO){
            experience = new Certificate();
//            experience.setConfirmed(experienceDTO.getConfirmed());
//            experience.setDescription(experience.getDescription());
//            experience.setIsDeleted(experienceDTO.getIsDeleted());
//            experience.setName(experienceDTO.getName());
//            experience.setUpdatedBy(experienceDTO.getUpdatedBy());
            ((Certificate)experience).setYear(((CertificateDTO)experienceDTO).getYear());
//            experience.setUser(userService.findById(experienceDTO.getUserId()));
        }
        if(experienceDTO instanceof JobExperienceDTO){
            experience = new JobExperience();
        }
        experience.setConfirmed(experienceDTO.getConfirmed());
        experience.setDescription(experience.getDescription());
        experience.setIsDeleted(experienceDTO.getIsDeleted());
        experience.setName(experienceDTO.getName());
        experience.setUpdatedBy(experienceDTO.getUpdatedBy());
        experience.setUser(userService.findById(experienceDTO.getUserId()));
        return experience;

    }

    public Feedback feedbackDTOToFeedback(FeedbackDTO feedbackDTO){
        return Feedback.builder()
                .rate(feedbackDTO.getRate())
                .content(feedbackDTO.getContent())
                .description(feedbackDTO.getDescription())
                .createdBy(feedbackDTO.getCreatedBy())
                .updatedBy(feedbackDTO.getUpdatedBy())
                .studying(studyingService.findById(feedbackDTO.getStudyingId()))
                .build();
    }

    public Discount discountDTOToDiscount(DiscountDTO discountDTO){
        Discount discount = null;

        long am = ((AmountDiscountDTO) discountDTO).getAmount();
        boolean b = true;
        if((discountDTO) instanceof AmountDiscountDTO){
            discount = new AmountDiscount();
            ((AmountDiscount) discount).setAmount(((AmountDiscountDTO) discountDTO).getAmount());
        }
        if((discountDTO) instanceof PercentageDiscountDTO){
            discount = new PercentageDiscount();
            ((PercentageDiscount) discount).setPercentage(((PercentageDiscountDTO) discountDTO).getPercentage());
        }
        discount.setCreatedBy(discountDTO.getCreatedBy());
        discount.setDescription(discountDTO.getDescription());
        discount.setIsDeleted(discountDTO.getIsDeleted());
        discount.setStatus(discountDTO.getStatus());
        discount.setUpdatedBy(discountDTO.getUpdatedBy());
        discount.setStudyings(studyingService.findMany(discountDTO.getStudyings()));
        discount.setValidFrom(HelperService.stringToInstant(discountDTO.getValidFrom()));
        discount.setValidTo(HelperService.stringToInstant(discountDTO.getValidTo()));

//        Discount discount = Discount.builder().name(discountDTO.getName())
//                .status(discountDTO.getStatus())
//                .validFrom(discountDTO.getValidFrom())
//                .validTo(discountDTO.getValidTo())
//                .build();
//        discount.setDescription(discountDTO.getDescription());
//        discount.setIsDeleted(discountDTO.getIsDeleted());
        return discount;
    }
}
