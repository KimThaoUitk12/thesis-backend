package com.learning.uit.repository;

import com.learning.uit.model.base.Discount;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Repository
public interface DiscountRepository extends JpaRepository<Discount, String> {
    @Query(value = "SELECT * FROM discount where is_deleted = 0", nativeQuery = true)
    List<Discount> findAll();

    @Query(value = "SELECT * FROM discount where is_deleted = 0 AND id = ?1", nativeQuery = true)
    Optional<Discount> findById(String id);

    @Query(value = "SELECT * FROM discount WHERE is_deleted = 0 AND name LIKE %?1%", nativeQuery = true)
    Collection<Discount> findByName(String name);


}
