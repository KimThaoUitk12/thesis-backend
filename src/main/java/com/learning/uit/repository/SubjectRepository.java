package com.learning.uit.repository;

import com.learning.uit.model.Subject;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Repository
public interface SubjectRepository extends JpaRepository<Subject, String> {
    @Query(value = "SELECT * FROM subject WHERE is_deleted = 0", nativeQuery = true)
    List<Subject> findAll();

    @Query(value = "SELECT * FROM subject WHERE is_deleted = 0 AND ID = ?1", nativeQuery = true)
    Optional<Subject> findById(String id);

    @Query(value = "SELECT * FROM subject WHERE is_deleted = 0 AND name LIKE %?1%", nativeQuery = true)
    Collection<Subject> findByName(String name);

    @Query(value = "SELECT * FROM subject WHERE is_deleted = 0 AND category LIKE %?1%", nativeQuery = true)
    Collection<Subject> findByCategory(String category);

    @Query(value = "SELECT * FROM subject WHERE is_deleted = 0 AND code = ?1", nativeQuery = true)
    Subject findByCode(String code);
}
