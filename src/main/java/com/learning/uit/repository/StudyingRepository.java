package com.learning.uit.repository;

import com.learning.uit.model.Studying;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface StudyingRepository extends JpaRepository<Studying, String> {
    @Query(value = "SELECT * FROM studying WHERE is_deleted = 0", nativeQuery = true)
    List<Studying> findAll();

    @Query(value = "SELECT * FROM studying WHERE is_deleted = 0 AND id = ?1", nativeQuery = true)
    Optional<Studying> findById(String id);
}
