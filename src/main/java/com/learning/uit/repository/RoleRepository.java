package com.learning.uit.repository;

import com.learning.uit.common.ERole;
import com.learning.uit.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository<Role, Integer> {
//    Optional<Role> findByName(ERole name);
    @Query(value = "SELECT r.e_role FROM role r JOIN user_role ur ON r.id = ur.role_id JOIN user u ON u.id = ur.user_id" +
            "WHERE u.id = ?1", nativeQuery = true)
    Collection<ERole> findRoleByUserId(String userId);
}
