package com.learning.uit.repository;

import com.learning.uit.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, String>{
    @Query(value = "SELECT * FROM user WHERE (first_name like %?1% or last_name like %?1%) AND is_deleted = 0", nativeQuery = true)
    Collection<User> findByName(String name);

    @Query(value = "SELECT * FROM user WHERE is_deleted = 0", nativeQuery = true)
    Collection<User> findAllByUser();

    @Query(value = "SELECT * FROM user WHERE email LIKE ?1 AND is_deleted = 0", nativeQuery = true)
    User findByEmail(String email);

    @Query(value = "SELECT * FROM user WHERE is_deleted = 0 AND id = ?1", nativeQuery = true)
    Optional<User> findById(String id);
}
