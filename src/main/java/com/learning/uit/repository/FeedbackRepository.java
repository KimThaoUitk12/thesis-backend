package com.learning.uit.repository;

import com.learning.uit.model.Feedback;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Repository
public interface FeedbackRepository extends JpaRepository<Feedback, String> {
    @Query(value = "SELECT * FROM feedback WHERE is_deleted = 0 AND id = ?1", nativeQuery = true)
    Optional<Feedback> findById(String id);

    @Query(value = "SELECT * FROM feedback WHERE is_deleted = 0", nativeQuery = true)
    List<Feedback> findAll();

    @Query(value = "SELECT * " +
            "FROM feedback f join studying s on f.studying_id = s.id " +
            "inner join user ss ON ss.id = s.user_id " +
            "WHERE f.is_deleted = 0 AND ss.id = ?1", nativeQuery = true)
    Collection<Feedback> findByUser(String userId);

    @Query(value = "SELECT * FROM feedback f JOIN studying S ON f.studying_id = s.id " +
            "inner JOIN specific_class sc ON sc.id = s.class_id " +
            "WHERE f.is_deleted = 0 AND sc.id = ?1", nativeQuery = true)
    Collection<Feedback> findByClass(String classId);

    @Query(value = "SELECT * FROM feedback f JOIN studying s ON f.studying_id = s.id " +
            "JOIN specific_class sc ON sc.id = s.class_id JOIN course c ON c.ID = sc.course_id " +
            "WHERE f.is_deleted = 0 AND c.id = ?1", nativeQuery = true)
    Collection<Feedback> findByCourse(String courseId);

}
