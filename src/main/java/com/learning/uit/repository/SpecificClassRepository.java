package com.learning.uit.repository;

import com.learning.uit.model.SpecificClass;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

@Repository
public interface SpecificClassRepository extends JpaRepository<SpecificClass, String> {
    @Query(value = "SELECT * FROM specific_class WHERE is_deleted = 0", nativeQuery = true)
    List<SpecificClass> findAll();

    @Query(value = "SELECT * FROM Specific_class WHERE is_deleted = 0 AND name LIKE %?1%", nativeQuery = true)
    Collection<SpecificClass> findByName(String id);

    @Query(value = "SELECT * FROM specific_class WHERE is_deleted = 0 AND course_id = ?1", nativeQuery = true)
    Collection<SpecificClass> findByCourse(String courseId);

    @Query(value = "SELECT * FROM specific_class WHERE is_deleted = 0 AND status = ?1", nativeQuery = true)
    Collection<SpecificClass> findByStatus(int status);
}
