package com.learning.uit.repository;

import com.learning.uit.model.Schedule;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Repository
public interface ScheduleRepository extends JpaRepository<Schedule, String> {
    @Query(value = "SELECT * FROM schedule WHERE is_deleted = 0", nativeQuery = true)
    List<Schedule> findAll();

    @Query(value = "SELECT * FROM schedule WHERE is_deleted = 0 AND class_id = ?1", nativeQuery = true)
    Collection<Schedule> findByClass(String classId);

    @Query(value = "SELECT * FROM schedule WHERE is_deleted = 0 AND ID = ?1", nativeQuery = true)
    Optional<Schedule> findById(String id);
}
