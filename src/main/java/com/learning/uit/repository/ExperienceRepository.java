package com.learning.uit.repository;

import com.learning.uit.model.base.Experience;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Repository
public interface ExperienceRepository extends JpaRepository<Experience, String> {
    @Query(value = "SELECT * FROM experience WHERE is_deleted = 0", nativeQuery = true)
    List<Experience> findAll();

    @Query(value = "SELECT * FROM experience WHERE is_deleted = 0 and ID = ?1", nativeQuery = true)
    Optional<Experience> findById(String id);

    @Query(value = "SELECT * FROM experience WHERE is_deleted = 0 AND name LIKE %?1%", nativeQuery = true)
    Collection<Experience> findByName(String name);

    @Query(value = "SELECT * FROM experience WHERE is_deleted = 0 AND year = ?1", nativeQuery = true)
    Collection<Experience> findByValid(int from);
}
