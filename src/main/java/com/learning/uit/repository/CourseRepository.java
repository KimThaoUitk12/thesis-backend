package com.learning.uit.repository;

import com.learning.uit.model.Course;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Repository
public interface CourseRepository extends JpaRepository<Course, String> {
    @Query(value = "SELECT * FROM course WHERE is_deleted = 0",nativeQuery = true)
    List<Course> findAll();

    @Query(value = "SELECT * FROM course WHERE is_deleted = 0 AND id = ?1", nativeQuery = true)
    Optional<Course> findById(String id);

    @Query(value = "SELECT * FROM course WHERE is_deleted = 0 AND name LIKE %?1%", nativeQuery = true)
    Collection<Course> findByName(String name);
}
