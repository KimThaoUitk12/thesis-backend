package com.learning.uit.repository;

import com.learning.uit.model.Teaching;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TeachingRepository extends JpaRepository<Teaching, String> {
    @Query(value = "SELECT * FROM teaching WHERE is_deleted = 0", nativeQuery = true)
    List<Teaching> findAll();

    @Query(value = "SELECT * FROM teaching WHERE is_deleted = 0 AND ID = ?1", nativeQuery = true)
    Optional<Teaching> findById(String id);
}
