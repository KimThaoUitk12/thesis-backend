package com.learning.uit.repository;

import com.learning.uit.model.Session;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface SessionRepository extends JpaRepository<Session, String> {
    @Query(value = "SELECT * FROM session WHERE is_deleted = 0", nativeQuery = true)
    List<Session> findAll();

    @Query(value = "SELECT * FROM session WHERE is_deleted = 0 AND ID = ?1", nativeQuery = true)
    Optional<Session> findById(String id);
}
