package com.learning.uit.schedule;

import com.learning.uit.common.Constants;
import com.learning.uit.model.SpecificClass;
import com.learning.uit.service.SpecificClassService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import java.time.Instant;
import java.util.Collection;
import java.util.stream.Collectors;

@Configuration
@EnableScheduling
public class TaskRunner {

    @Autowired
    private SpecificClassService specificClassService;

    @Scheduled(fixedDelay = 1000 * 60)
    public void updateClosedStatusOfClass(){
        Collection<SpecificClass> classes = specificClassService.findAll()
                .stream()
                .filter(specificClass -> specificClass.getStatus().equals(Constants.CLASS_READY)
                        || specificClass.getStatus().equals(Constants.CLASS_PROCESS))
                .collect(Collectors.toList());
        for(SpecificClass clazz : classes){
            if(clazz.getEnd() == null){
                break;
            }
            if (clazz.getEnd().compareTo(Instant.now()) < 0){
                clazz.setStatus(Constants.CLASS_CLOSED);
            }
        }
    }
}
