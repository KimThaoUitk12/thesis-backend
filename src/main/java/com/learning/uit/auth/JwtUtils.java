package com.learning.uit.auth;

import io.jsonwebtoken.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import java.security.SignatureException;
import java.util.Date;

@Component
public class JwtUtils {

    private static final Logger logger = (Logger) LoggerFactory.getLogger(JwtUtils.class);

    @Value("${bezkoder.app.jwtSecret}")
    private String jwtSecret;

    @Value("${bezkoder.app.jwtExpirationMs}")
    private int jwtExpirationMs;

    public String generateJwtToken(Authentication authentication){
        UserDetailsImpl userPrincipal = (UserDetailsImpl) authentication.getPrincipal();

        return Jwts.builder()
                .setSubject(userPrincipal.getUsername())
                .setIssuedAt(new Date())
                .setExpiration(new Date(new Date().getTime() + jwtExpirationMs))
                .signWith(SignatureAlgorithm.HS512, jwtSecret)
                .compact();
    }

    public String getEmailFromToken(String authToken){
        return Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(authToken).getBody().getSubject();
    }

    public boolean validateToken(String authToken){
            try{
                Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(authToken);
                return true;
            }catch (MalformedJwtException e){
                logger.error("Invalid JWT token: {}", e.getMessage());
            }catch (ExpiredJwtException e){
                logger.error("JWT token is expired: {}", e.getMessage());
            }catch (UnsupportedJwtException e){
                logger.error("JWT token is unsupported: {}", e.getMessage());
            }catch (IllegalArgumentException e){
                logger.error("JWT claims String empty: {}", e.getMessage());
            }
            return false;
    }
}
