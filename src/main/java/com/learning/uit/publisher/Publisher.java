package com.learning.uit.publisher;

import com.learning.uit.events.NewCallSessionInitializationEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

@Component
public class Publisher {
    @Autowired
    ApplicationEventPublisher applicationEventPublisher;

    public void initClassWithId(String classId){
        applicationEventPublisher.publishEvent(new NewCallSessionInitializationEvent(this, classId));
    }
}
