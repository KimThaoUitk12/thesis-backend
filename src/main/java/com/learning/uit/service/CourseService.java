package com.learning.uit.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.learning.uit.dto.CourseDTO;
import com.learning.uit.model.Course;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.Collection;

@Service
public interface CourseService {
    Collection<Course> findAll();
    Course findById(String courseId);
    Collection<Course> findByName(String name);
    Collection<Course> findBySubject(String subjectId);
    Course findByClass(String classId);

    void deleteById(String courseId);
    Course create(String courseDTO, String file) throws JsonProcessingException;
    Course update(String id, CourseDTO courseDTO);
    long getRateByCourse(String courseId);

}
