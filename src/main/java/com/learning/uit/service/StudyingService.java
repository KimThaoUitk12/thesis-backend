package com.learning.uit.service;

import com.learning.uit.dto.StudyingDTO;
import com.learning.uit.model.Schedule;
import com.learning.uit.model.Studying;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public interface StudyingService {
    Collection<Studying> findAll();
    Studying findById(String id);
    Collection<Studying> findByUser(String userId);
    Studying findByClass(String classId);
    int delete(String id);

    void deleteById(String id);
    Studying create(StudyingDTO studyingDTO);
    Studying update(String id, StudyingDTO studyingDTO);

    Collection<Studying> findMany(Collection<String> studyings);
}
