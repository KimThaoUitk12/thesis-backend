package com.learning.uit.service;

import com.learning.uit.dto.base.ExperienceDTO;
import com.learning.uit.model.base.Experience;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public interface ExperienceService {
    Collection<Experience> findAll();
    Collection<Experience> findByName(String name);
    Collection<Experience> findByValid(int from);
    Experience findById(String id);
    Collection<Experience> findByUser(String userId);

    Experience deleteById(String id);
    Collection<Experience> saveMany(Collection<ExperienceDTO> experiences);
    Experience create(ExperienceDTO experience);
    Experience updateById(String id, ExperienceDTO experienceDTO);
}
