package com.learning.uit.service;

import com.learning.uit.dto.ScheduleDTO;
import com.learning.uit.model.Schedule;
import com.learning.uit.response.UpdateEndDateResponse;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public interface ScheduleService {
    Collection<Schedule> findAll();
    Schedule findById(String id);
    Collection<Schedule> findByClass(String classId);
    Collection<Schedule> findByTutor(String teacherId);
    Collection<Schedule> findByStudent(String studentId);
    Collection<Schedule> findManyByIds(Collection<String> ids);
    Collection<Schedule> findByCourse(String courseId);
    Collection<Schedule> findByDayOfWeekAndClass(String classId, int dayOfWeek);
    long getDurationSchedule(Schedule schedule);
    UpdateEndDateResponse updateEndDate(Schedule schedule, String action);

    boolean deleteById(String scheduleId);
    Schedule create(ScheduleDTO scheduleDTO);
    Schedule update(String scheduleId, ScheduleDTO scheduleDTO);

    boolean checkForLearning(Collection<Schedule> scheduleCollection, Schedule schedule);

    boolean checkDateConflict(Schedule s1, Schedule s2);

    Collection<Schedule> createMany(Collection<ScheduleDTO> scheduleDTOS);
}
