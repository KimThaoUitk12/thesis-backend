package com.learning.uit.service;

import com.learning.uit.dto.SubjectDTO;
import com.learning.uit.model.Subject;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public interface SubjectService {
    Collection<Subject> findAll();
    Collection<Subject> findByName(String name);
    Collection<Subject> findByCategory(String category);
    Subject findById(String id);
    Subject findByCode(String code);
    Subject findByClass(String classId);

    void deleteById(String id);
    Subject create(SubjectDTO subject);
    Subject update(String id, SubjectDTO subject);
}
