package com.learning.uit.service;

import com.learning.uit.dto.Account;
import com.learning.uit.dto.UpdateUserDTO;
import com.learning.uit.dto.UserDTO;
import com.learning.uit.model.User;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public interface UserService {
    Collection<User> findAllByUser();
    Collection<User> findByName(String name);
    User findByEmail(String email);
    Collection<User> findAll();
    Collection<User> findLearnerByCourse(String idCourse);
    Collection<User> findLearnerPaidCourse(String idCourse);
    Collection<User> findLearnerDidntPaidCourse(String idCourse);
    User findLearnerByClass(String classId);
    User findTeacherByClass(String classId);

    User deleteById(String id);
    User findById(String id);
    void deleteMany(Collection<String> ids);
    User updateByUser(String userId, UpdateUserDTO user);
    User updateByAdmin(String userId, UserDTO user);
    void updateAccount(String userId, Account account);
    User createUser(UserDTO userDTO);

}
