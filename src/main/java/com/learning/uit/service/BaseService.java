package com.learning.uit.service;

import com.learning.uit.model.Role;
import com.learning.uit.model.Teaching;
import com.learning.uit.model.User;
import com.learning.uit.model.base.BaseModel;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.ws.rs.ForbiddenException;

public interface BaseService {
    public void checkPermission(BaseModel baseModel);
}
