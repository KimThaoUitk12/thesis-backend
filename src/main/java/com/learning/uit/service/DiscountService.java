package com.learning.uit.service;

import com.learning.uit.dto.base.DiscountDTO;
import com.learning.uit.model.base.Discount;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public interface DiscountService {
    Collection<Discount> findAll();
    Discount findById(String discountId);
    Collection<Discount> findByName(String name);
    Collection<Discount> beetweenOfAmount(long lower, long upper);
    Collection<Discount> beetweenOfPercentage(double lower, double upper);

    void deleteById(String discountId);
    Discount create(DiscountDTO discountDTO);
    Discount updateById(String discountId, DiscountDTO discountDTO);

    Collection<Discount> findMany(Collection<String> discountIds);
}
