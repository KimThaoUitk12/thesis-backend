package com.learning.uit.service;

import com.learning.uit.dto.SessionDTO;
import com.learning.uit.model.Session;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public interface SessionService {
    Collection<Session> findAll();
    Session findById(String id);
    Collection<Session> findByClass(String classId);
    Collection<Session> findManyByIds(Collection<String> ids);

    void deleteById(String id);
    Session create(SessionDTO sessionDTO);
}
