package com.learning.uit.service;

import com.learning.uit.dto.ClassDTO;
import com.learning.uit.model.SpecificClass;
import com.learning.uit.model.User;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public interface SpecificClassService {
    Collection<SpecificClass> findAll();
    SpecificClass findById(String id);
    Collection<SpecificClass> findByName(String name);
    Collection<SpecificClass> findByStatus(int status);
    Collection<SpecificClass> findByTutor(String userId);
    Collection<SpecificClass> findByStudent(String userId);
    Collection<SpecificClass> findBySubject(String subjectId);
    Collection<SpecificClass> findByCourse(String courseId);
    Collection<SpecificClass> findManyByIds(Collection<String> id);
    long getClassDuration(String classId);

    SpecificClass deleteById(String specificClassId);
    SpecificClass updateById(String classId, ClassDTO classDTO);
    SpecificClass create(ClassDTO classDTO);
}
