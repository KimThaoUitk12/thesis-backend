package com.learning.uit.service.impl;

import com.learning.uit.auth.UserDetailsImpl;
import com.learning.uit.coverter.ConverterService;
import com.learning.uit.dto.base.DiscountDTO;
import com.learning.uit.helper.HelperService;
import com.learning.uit.model.*;
import com.learning.uit.model.base.BaseModel;
import com.learning.uit.model.base.Discount;
import com.learning.uit.repository.DiscountRepository;
import com.learning.uit.security.WebSecurityConfig;
import com.learning.uit.service.BaseService;
import com.learning.uit.service.DiscountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.ws.rs.ForbiddenException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Service
public class DiscountServiceImpl implements DiscountService, BaseService {

    @Autowired
    private DiscountRepository discountRepository;

    @Autowired
    private ConverterService converterService;

    @Autowired
    private HelperService helperService;

    @Override
    public Collection<Discount> findAll() {
        return discountRepository.findAll();
    }

    @Override
    public Discount findById(String discountId) {
        return discountRepository.findById(discountId).orElseThrow(() -> new RuntimeException("no discount has id: "+discountId));
    }

    @Override
    public Collection<Discount> findByName(String name) {
        return discountRepository.findByName(name);
    }

    @Override
    public Collection<Discount> beetweenOfAmount(long lower, long upper) {
        Collection<Discount> discounts = new ArrayList<>();
        for(Discount d : this.findAll()){
            if(d instanceof AmountDiscount){
                if(((AmountDiscount) d).getAmount() >= lower && ((AmountDiscount) d).getAmount() <= upper){
                    discounts.add(d);
                }
            }
        }
        return discounts.size() == 0 ? null : discounts;
    }

    @Override
    public Collection<Discount> beetweenOfPercentage(double lower, double upper) {
        Collection<Discount> discounts = new ArrayList<>();
        for(Discount d : this.findAll()){
            if(d instanceof PercentageDiscount){
                if(((PercentageDiscount) d).getPercentage() >= lower && ((PercentageDiscount) d).getPercentage() <= upper){
                    discounts.add(d);
                }
            }
        }
        return discounts.size() == 0 ? null : discounts;
    }

    @Override
    public void deleteById(String discountId) {
        Discount discount = this.findById(discountId);
        this.checkPermission(discount);
        discount.setIsDeleted(1);
        discount.setUpdatedAt(Instant.now());
        discount.setUpdatedBy(HelperService.getCurrentUser().getId());
        discountRepository.save(discount);
    }

    @Override
    public Discount create(DiscountDTO discountDTO) {
        Discount discount = converterService.discountDTOToDiscount(discountDTO);
        discount.setCreatedBy(HelperService.getCurrentUser().getId());
        this.checkPermission(discount);
        return discountRepository.save(discount);
    }

    @Override
    public Discount updateById(String discountId, DiscountDTO discountDTO) {
        Discount discount = this.findById(discountId);
        this.checkPermission(discount);
        discount = converterService.discountDTOToDiscount(discountDTO);
        discount.setUpdatedBy(HelperService.getCurrentUser().getId());
        discount.setUpdatedAt(Instant.now());
        return discountRepository.save(discount);
    }

    @Override
    public Collection<Discount> findMany(Collection<String> discountIds) {
        if(discountIds == null){
            return null;
        }
        List<Discount> list = new ArrayList<Discount>();
        for(String discountId : discountIds){
            list.add(this.findById(discountId));
        }
        return list;
    }

    @Override
    public void checkPermission(BaseModel baseModel) {
        UserDetailsImpl con = HelperService.getCurrentUser();

        boolean isAdmin = false;

        for(GrantedAuthority authority : con.getAuthorities()){
            if(authority.getAuthority().equals("ROLE_ADMIN")){
                isAdmin = true;
            }
        }

        if(!(con.getId().equals(((Discount)baseModel).getCreatedBy()))){
            throw new ForbiddenException();
        }
    }
}
