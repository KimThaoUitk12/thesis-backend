package com.learning.uit.service.impl;

import com.learning.uit.auth.UserDetailsImpl;
import com.learning.uit.common.Constants;
import com.learning.uit.coverter.ConverterService;
import com.learning.uit.dto.StudyingDTO;
import com.learning.uit.exception.PrivilegeException;
import com.learning.uit.exception.ScheduleConflictException;
import com.learning.uit.exception.SubcribeException;
import com.learning.uit.helper.HelperService;
import com.learning.uit.model.*;
import com.learning.uit.model.base.BaseModel;
import com.learning.uit.repository.SpecificClassRepository;
import com.learning.uit.repository.StudyingRepository;
import com.learning.uit.security.WebSecurityConfig;
import com.learning.uit.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.ws.rs.ForbiddenException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collection;

@Service
public class StudyingServiceImpl implements StudyingService, BaseService {

    @Autowired
    private StudyingRepository studyingRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private ConverterService converterService;

    @Autowired
    private SpecificClassRepository specificClassRepository;

    @Autowired
    private StudyingService studyingService;

    @Autowired
    private TeachingService teachingService;

    @Autowired
    private ScheduleService scheduleService;

    @Autowired
    private SpecificClassService specificClassService;

    @Autowired
    private HelperService helperService;

    @Override
    public Collection<Studying> findAll() {
        return studyingRepository.findAll();
    }

    @Override
    public Studying findById(String studyingId) {
        return studyingRepository.findById(studyingId).orElseThrow(() -> new RuntimeException("no studying has id: " + studyingId));
    }

    @Override
    public Collection<Studying> findByUser(String userId) {
        return userService.findById(userId).getStudyings();
    }

    @Override
    public Studying findByClass(String classId) {
        return this.findAll().stream().filter(studying -> studying.getSpecificClass().getId().equals(classId)).findFirst().get();
    }

    @Override
    public int delete(String id) {
        Studying studying = this.findById(id);
        SpecificClass clazz = studying.getSpecificClass();
        User student = studying.getUser();
        User teacher = userService.findTeacherByClass(clazz.getId());
        if(student == null && teacher == null){
            return -1;
        }
        String current = HelperService.getCurrentUser().getId();
        if(teacher != null){
            if(teacher.getId().equals(HelperService.getCurrentUser().getId())){
                studying.setTeacherDel(1);
                studyingRepository.save(studying);
            }
        }

        if(student != null){
            if(student.getId().equals(HelperService.getCurrentUser().getId())){
                studying.setStudentDel(1);
                studyingRepository.save(studying);
            }
        }

        if(studying.getStudentDel() == 1 && studying.getTeacherDel() == 1){
            studyingService.deleteById(studying.getId());
            clazz.setIsDeleted(1);
            specificClassRepository.save(clazz);
            return 1;
        }
        return 0;
    }

    @Override
    public void deleteById(String id) {
        Studying studying = this.findById(id);
//        this.checkPermission(studying);
        studying.setIsDeleted(1);
        studying.setUpdatedAt(Instant.now());
        studying.setUpdatedBy(HelperService.getCurrentUser().getId());
        studyingRepository.save(studying);
    }

    @Override
    public Studying create(StudyingDTO studyingDTO) {
        String updater = HelperService.getCurrentUser().getId();
        User student = userService.findLearnerByClass(studyingDTO.getSpecificClassId());
        studyingDTO.setUserId(updater);
        if (student != null){
            throw  new SubcribeException("this class has been subcribed by another one");
        }

        if (!specificClassService.findById(studyingDTO.getSpecificClassId()).getStatus().equals(Constants.CLASS_READY)){
            throw  new SubcribeException("this class is not ready");
        }
//        Studying studying = studyingService.findByClass(studyingDTO.getSpecificClassId());
        Studying studying = converterService.StudyingDTOToStudying(studyingDTO);
//        this.checkPermission(studying);
//        Teaching teaching = teachingService.findByClass(studyingDTO.getSpecificClassId());
//        Collection<Schedule> studentSchedules = scheduleService.findByStudent(studying.getUser().getId());
//        Collection<Schedule> tutorSchedules = scheduleService.findByTutor(teaching.getUser().getId());
//        for(Schedule schedule : tutorSchedules){
//            if(scheduleService.checkForLearning(studentSchedules, schedule) == false){
//                throw new ScheduleConflictException("schedule conflicts");
//            }
//        }
        this.checkScheduleStudyingConflict(studyingDTO.getSpecificClassId());

        SpecificClass specificClass = specificClassService.findById(studying.getSpecificClass().getId());
        specificClass.setStatus(Constants.CLASS_PROCESS);
        specificClassRepository.save(specificClass);


        studying.setDescription(studyingDTO.getDescription());
        studying.setCreatedBy(updater);
        studying.setUpdatedBy(updater);
        return studyingRepository.save(studying);
    }

    @Override
    public Studying update(String id, StudyingDTO studyingDTO) {
        Studying studying = this.findById(id);
        this.checkPermission(studying);
        studying.setDescription(studyingDTO.getDescription());
        studying.setIsDeleted(1);
//        studying.setDiscounts(studyingDTO.getDiscounts());
        studying.setPaid(studyingDTO.getPaid());
        studying.setRefund(studyingDTO.getRefund());
        studying.setUpdatedAt(Instant.now());
        studying.setUpdatedBy(HelperService.getCurrentUser().getId());
        return studyingRepository.save(studying);
    }

    @Override
    public Collection<Studying> findMany(Collection<String> studyings) {
        Collection<Studying> collection = new ArrayList<>();
        for(String studyingId : studyings){
            collection.add(this.findById(studyingId));
        }
        return collection;
    }

    @Override
    public void checkPermission(BaseModel baseModel) {
        UserDetailsImpl con = HelperService.getCurrentUser();

        boolean isAdmin = false;

        for(GrantedAuthority authority : con.getAuthorities()){
            if(authority.getAuthority().equals("ROLE_ADMIN")){
                isAdmin = true;
            }
        }

        String teacherId = null;
        User teachingUser = userService.findTeacherByClass(((Studying)baseModel).getSpecificClass().getId());
        if(teachingUser != null){
            teacherId = teachingUser.getId();
        }

        if (teacherId.equals(con.getId())){
            throw new PrivilegeException("you can not be both teacher and student in same class");
        }

        if(!isAdmin){
            throw new PrivilegeException("you do not have permission to change");
        }
    }

    boolean checkScheduleStudyingConflict(String classId){
        Collection<Schedule> studentSchedules = scheduleService.findByStudent(HelperService.getCurrentUser().getId());
        Collection<Schedule> classSchedules = scheduleService.findByClass(classId);

        if(studentSchedules == null || classSchedules == null){
            return false;
        }
        
        for (Schedule s : studentSchedules){
            for (Schedule c : classSchedules){
                if(scheduleService.checkDateConflict(s,c)){
                    return true;
                }
            }
        }
        
        return false;
    }
}
