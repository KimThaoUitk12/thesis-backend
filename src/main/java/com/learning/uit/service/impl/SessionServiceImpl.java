package com.learning.uit.service.impl;

import com.learning.uit.auth.UserDetailsImpl;
import com.learning.uit.dto.SessionDTO;
import com.learning.uit.helper.HelperService;
import com.learning.uit.model.Role;
import com.learning.uit.model.Session;
import com.learning.uit.model.SpecificClass;
import com.learning.uit.model.User;
import com.learning.uit.model.base.BaseModel;
import com.learning.uit.repository.SessionRepository;
import com.learning.uit.security.WebSecurityConfig;
import com.learning.uit.service.BaseService;
import com.learning.uit.service.SessionService;
import com.learning.uit.service.SpecificClassService;
import com.learning.uit.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.ws.rs.ForbiddenException;
import java.time.Instant;
import java.util.Collection;

@Service
public class SessionServiceImpl implements SessionService, BaseService {

    @Autowired
    private SessionRepository sessionRepository;

    @Autowired
    private SpecificClassService specificClassService;

    @Autowired
    private UserService userService;

    @Autowired
    private HelperService helperService;

    @Override
    public Collection<Session> findAll() {
        return sessionRepository.findAll();
    }

    @Override
    public Session findById(String id) {
        return sessionRepository.findById(id).orElseThrow(() -> new RuntimeException("no session has id: " + id));
    }

    @Override
    public Collection<Session> findByClass(String classId) {
        return specificClassService.findById(classId).getSessions();
    }

    @Override
    public Collection<Session> findManyByIds(Collection<String> ids) {
        Collection<Session> sessions = null;
        for(String id : ids){
            sessions.add(this.findById(id));
        }
        return null;
    }

    @Override
    public void deleteById(String id) {
        Session session = this.findById(id);
        this.checkPermission(session);
        session.setIsDeleted(1);
        session.setUpdatedAt(Instant.now());
        session.setUpdatedBy(HelperService.getCurrentUser().getId());
        sessionRepository.save(session);
    }

    @Override
    public Session create(SessionDTO sessionDTO) {
        Session session = Session.builder()
                .duration(sessionDTO.getDuration())
                .endAt(HelperService.stringToInstant(sessionDTO.getEndAt()))
                .startAt(HelperService.stringToInstant(sessionDTO.getStartAt()))
                .specificClass(sessionDTO.getSpecificClass())
//                .videos(sessionDTO.getVideos())
                .build();
        this.checkPermission(session);
        session.setCreatedBy(HelperService.getCurrentUser().getId());
        session.setUpdatedBy(HelperService.getCurrentUser().getId());
        session.setDescription(sessionDTO.getDescription());
        return sessionRepository.save(session);
    }

    @Override
    public void checkPermission(BaseModel baseModel) {
        UserDetailsImpl con = HelperService.getCurrentUser();

        boolean isAdmin = false;

        for(GrantedAuthority authority : con.getAuthorities()){
            if(authority.getAuthority().equals("ROLE_ADMIN")){
                isAdmin = true;
            }
        }

        String teacherId = userService.findTeacherByClass(((Session)baseModel).getSpecificClass().getId()).getId();

        if(!(con.getId().equals(teacherId) || isAdmin)){
            throw new ForbiddenException();
        }
    }
}
