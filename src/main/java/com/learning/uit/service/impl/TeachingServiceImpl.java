package com.learning.uit.service.impl;

import com.learning.uit.auth.UserDetailsImpl;
import com.learning.uit.coverter.ConverterService;
import com.learning.uit.dto.TeachingDTO;
import com.learning.uit.helper.HelperService;
import com.learning.uit.model.Role;
import com.learning.uit.model.Teaching;
import com.learning.uit.model.User;
import com.learning.uit.model.base.BaseModel;
import com.learning.uit.repository.TeachingRepository;
import com.learning.uit.security.WebSecurityConfig;
import com.learning.uit.service.BaseService;
import com.learning.uit.service.TeachingService;
import com.learning.uit.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.ws.rs.ForbiddenException;
import java.time.Instant;
import java.util.Collection;

@Service
public class TeachingServiceImpl implements TeachingService, BaseService {

    @Autowired
    private TeachingRepository teachingRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private ConverterService converterService;

    @Autowired
    private HelperService helperService;

    @Override
    public Collection<Teaching> findAll() {
        return teachingRepository.findAll();
    }

    @Override
    public Teaching findById(String id) {
        return teachingRepository.findById(id).orElseThrow(() -> new RuntimeException("no teaching has id: " + id));
    }

    @Override
    public Collection<Teaching> findByUser(User user) {
        return userService.findById(user.getId()).getTeachings();
    }

    @Override
    public Teaching findByClass(String classId) {
        return this.findAll().stream().filter(teaching -> teaching.getSpecificClass().getId().equals(classId)).findFirst().get();
    }

    @Override
    public Collection<Teaching> findReceivedByUser(int receipt, String teacherId) {
        Collection<Teaching> teachings = (Collection<Teaching>) this.findAll()
                .stream()
                .filter(teaching -> teaching.getUser().getId() == teacherId && teaching.getReceived() == receipt);
        return teachings.size() == 0 ? null : teachings;
    }

    @Override
    public Teaching create(TeachingDTO teachingDTO) {
        Teaching teaching = converterService.TeachingDTOToTeaching(teachingDTO);
        teaching.setCreatedBy(HelperService.getCurrentUser().getId());
        return teachingRepository.save(teaching);
    }

    @Override
    public Teaching update(String classId, TeachingDTO teachingDTO) {
        Teaching teaching = this.findByClass(classId);
        this.checkPermission(teaching);
        teaching.setReceived(teachingDTO.getReceived());
        teaching.setDescription(teachingDTO.getDescription());
        teaching.setIsDeleted(teachingDTO.getIsDeleted());
        teaching.setUpdatedAt(Instant.now());
        teaching.setUpdatedBy(HelperService.getCurrentUser().getId());
        return teachingRepository.save(teaching);
    }

    @Override
    public void delete(Teaching teaching) {
        teachingRepository.delete(teaching);
    }

    @Override
    public void checkPermission(BaseModel baseModel) {
        UserDetailsImpl con = HelperService.getCurrentUser();

        boolean isAdmin = false;

        for(GrantedAuthority authority : con.getAuthorities()){
            if(authority.getAuthority().equals("ROLE_ADMIN")){
                isAdmin = true;
            }
        }

        if(!(con.getId().equals(((Teaching)baseModel).getUser().getId()) || isAdmin)){
            throw new ForbiddenException();
        }
    }
}
