package com.learning.uit.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.learning.uit.auth.UserDetailsImpl;
import com.learning.uit.coverter.ConverterService;
import com.learning.uit.dto.CourseDTO;
import com.learning.uit.helper.HelperService;
import com.learning.uit.model.*;
import com.learning.uit.model.base.BaseModel;
import com.learning.uit.repository.CourseRepository;
import com.learning.uit.security.WebSecurityConfig;
import com.learning.uit.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.time.Instant;
import java.util.Collection;
import java.util.stream.Collectors;
import javax.ws.rs.ForbiddenException;
@Service
@Transactional
public class CourseServiceImpl implements CourseService, BaseService {

    @Autowired
    private CourseRepository courseRepository;

    @Autowired
    private ConverterService converterService;

    @Autowired
    private SubjectService subjectService;

    @Autowired
    private UserService userService;

    @Autowired
    private HelperService helperService;

    @Autowired
    private FeedbackService feedbackService;

    @Autowired
    private SpecificClassService specificClassService;

    @Override
    public Collection<Course> findAll() {
        return courseRepository.findAll();
    }

    @Override
    public Course findById(String courseId) {
        return courseRepository.findById(courseId).orElseThrow(() -> new RuntimeException("no course has id: "+courseId));
    }

    @Override
    public Collection<Course> findByName(String name) {
        return courseRepository.findByName(name);
    }

    @Override
    public Collection<Course> findBySubject(String subjectId) {
        return courseRepository.findAll().stream().filter(course -> course.getSubject().getId().equals(subjectId)).collect(Collectors.toList());
    }

    @Override
    public Course findByClass(String classId) {
        return specificClassService.findById(classId).getCourse();
    }

    @Override
    public void deleteById(String courseId) {
        Course course = this.findById(courseId);
        this.checkPermission(course);
        course.setIsDeleted(1);
        course.setCreatedAt(Instant.now());
        course.setUpdatedBy(HelperService.getCurrentUser().getId());
        courseRepository.save(course);
    }

    @Override
    public Course create(String strCourse, String fileName) throws JsonProcessingException {
        CourseDTO courseDTO = new ObjectMapper().readValue(strCourse, CourseDTO.class);
        Course course = converterService.courseDTOToCourse(courseDTO);
        course.setUpdatedAt(Instant.now());
        course.setCreatedAt(Instant.now());
        course.setDescription(courseDTO.getDescription());
        course.setIntroVideo(fileName);
        course.setCreatedBy(HelperService.getCurrentUser().getId());
        return courseRepository.save(course);
    }

    @Override
    public Course update(String id, CourseDTO courseDTO) {
        Course course = this.findById(id);

        this.checkPermission(course);

        course.setDuration(courseDTO.getDuration());
        course.setFee(courseDTO.getFee());
        course.setName(courseDTO.getName());
        course.setIntroVideo(courseDTO.getIntroVideo());
        course.setSubject(subjectService.findById(courseDTO.getSubjectId()));
        course.setSyllabus(courseDTO.getSyllabus());
//        course.setUser(userService.findById(courseDTO.getUserId()));
        course.setDescription(courseDTO.getDescription());
        course.setUpdatedAt(Instant.now());
        course.setUpdatedBy(courseDTO.getUpdatedBy());
        return courseRepository.save(course);
    }

    @Override
    public long getRateByCourse(String courseId) {
        int sum = 0;
        Collection<Feedback> feedbacks = feedbackService.findByCourse(courseId);
        for (Feedback feedback : feedbacks){
            sum += feedback.getRate();
        }
        return feedbacks.size() != 0 ? sum / feedbacks.size() : 0;
    }

    @Override
    public void checkPermission(BaseModel baseModel) {
        UserDetailsImpl con = HelperService.getCurrentUser();

        boolean isAdmin = false;

        for(GrantedAuthority authority : con.getAuthorities()){
            if(authority.getAuthority().equals("ROLE_ADMIN")){
                isAdmin = true;
            }
        }

        if(!(con.getId().equals(((Course)baseModel).getCreatedBy()) || isAdmin)){
            throw new ForbiddenException();
        }
    }
}
