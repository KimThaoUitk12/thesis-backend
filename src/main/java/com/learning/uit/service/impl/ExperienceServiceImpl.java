package com.learning.uit.service.impl;

import com.learning.uit.auth.UserDetailsImpl;
import com.learning.uit.coverter.ConverterService;
import com.learning.uit.dto.CertificateDTO;
import com.learning.uit.dto.base.ExperienceDTO;
import com.learning.uit.helper.HelperService;
import com.learning.uit.model.*;
import com.learning.uit.model.base.BaseModel;
import com.learning.uit.model.base.Experience;
import com.learning.uit.repository.ExperienceRepository;
import com.learning.uit.security.WebSecurityConfig;
import com.learning.uit.service.BaseService;
import com.learning.uit.service.ExperienceService;
import com.learning.uit.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import javax.ws.rs.ForbiddenException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collection;
import java.util.stream.Collectors;

@Service
@Transactional
public class ExperienceServiceImpl implements ExperienceService, BaseService {

    @Autowired
    private ExperienceRepository experienceRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private ConverterService converterService;

    @Autowired
    private HelperService helperService;

    @Override
    public Collection<Experience> findAll() {
        return experienceRepository.findAll();
    }

    @Override
    public Collection<Experience> findByName(String name) {
        return experienceRepository.findByName(name);
    }

    @Override
    public Collection<Experience> findByValid(int from) {
        return experienceRepository.findByValid(from);
    }

    @Override
    public Experience findById(String id) {
        return experienceRepository.findById(id).get();
    }

    @Override
    public Collection<Experience> findByUser(String userId) {
        return this.findAll().stream().filter(experience -> experience.getUser().getId().equals(userId)).collect(Collectors.toList());
    }

    @Override
    public Experience deleteById(String id) {
        Experience experience = this.findById(id);
        this.checkPermission(experience);
        experience.setIsDeleted(1);
        experience.setUpdatedAt(Instant.now());
        experience.setUpdatedBy(HelperService.getCurrentUser().getId());
        return experienceRepository.save(experience);
    }

    @Override
    public Collection<Experience> saveMany(Collection<ExperienceDTO> experiences) {
        for(ExperienceDTO experience : experiences){
            this.create(experience);
        }
        return experiences.stream().map(experience -> this.create(experience)).collect(Collectors.toCollection(ArrayList::new));
    }

    @Override
    public Experience create(ExperienceDTO experienceDTO) {
//        Experience ex = Experience.builder()
//                .name(experienceDTO.getName())
//                .user(userService.findById(experienceDTO.getUserId())).build();
        Experience ex = converterService.experienceDTOToExperience(experienceDTO);
        this.checkPermission(ex);
        ex.setUpdatedBy(HelperService.getCurrentUser().getId());
//        if(experienceDTO instanceof CertificateDTO){
//            ((Certificate)ex).setYear(((CertificateDTO) experienceDTO).getYear());
//        }
        return experienceRepository.save(ex);
    }

    @Override
    public Experience updateById(String id, ExperienceDTO experienceDTO) {
        Experience experience = this.findById(id);
        this.checkPermission(experience);
        if(experience instanceof Certificate){
            experience = ((Certificate)experience);
            ((Certificate) experience).setYear(((Certificate) experience).getYear());
        }else {
            experience = ((JobExperience)experience);
        }
        experience.setUpdatedBy(HelperService.getCurrentUser().getId());
        experience.setUpdatedAt(Instant.now());
        experience.setName(experienceDTO.getName());
        experience.setDescription(experienceDTO.getDescription());
        experience.setConfirmed(experience.getConfirmed());
        experience.setIsDeleted(experience.getIsDeleted());
        experience.setUser(userService.findById(experienceDTO.getUserId()));
        return experienceRepository.save(experience);
    }

    @Override
    public void checkPermission(BaseModel baseModel) {
        UserDetailsImpl con = HelperService.getCurrentUser();

        boolean isAdmin = false;

        for(GrantedAuthority authority : con.getAuthorities()){
            if(authority.getAuthority().equals("ROLE_ADMIN")){
                isAdmin = true;
            }
        }

        if(!(con.getId().equals(((Experience)baseModel).getUser().getId()) || isAdmin)){
            throw new ForbiddenException();
        }
    }
}
