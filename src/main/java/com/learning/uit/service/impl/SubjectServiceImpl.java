package com.learning.uit.service.impl;

import com.learning.uit.auth.UserDetailsImpl;
import com.learning.uit.coverter.ConverterService;
import com.learning.uit.dto.SubjectDTO;
import com.learning.uit.helper.HelperService;
import com.learning.uit.model.Course;
import com.learning.uit.model.Role;
import com.learning.uit.model.Subject;
import com.learning.uit.model.User;
import com.learning.uit.model.base.BaseModel;
import com.learning.uit.repository.SubjectRepository;
import com.learning.uit.security.WebSecurityConfig;
import com.learning.uit.service.BaseService;
import com.learning.uit.service.SpecificClassService;
import com.learning.uit.service.SubjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import javax.ws.rs.ForbiddenException;
import java.time.Instant;
import java.util.Collection;

@Service
@Transactional
public class SubjectServiceImpl implements SubjectService, BaseService {

    @Autowired
    private SubjectRepository subjectRepository;

    @Autowired
    private ConverterService converterService;

    @Autowired
    private SpecificClassService specificClassService;

    @Autowired
    private HelperService helperService;

    @Override
    public Collection<Subject> findAll() {
        return subjectRepository.findAll();
    }

    @Override
    public Collection<Subject> findByName(String name) {
        return subjectRepository.findByName(name);
    }

    @Override
    public Collection<Subject> findByCategory(String category) {
        return subjectRepository.findByCategory(category);
    }

    @Override
    public Subject findById(String id) {
        return subjectRepository.findById(id).orElseThrow(() -> new RuntimeException("no subject has id: " + id));
    }

    @Override
    public Subject findByCode(String code) {
        return subjectRepository.findByCode(code);
    }

    @Override
    public Subject findByClass(String classId) {
        return specificClassService.findById(classId).getCourse().getSubject();
    }

    @Override
    public void deleteById(String id) {
        Subject subject = this.findById(id);
        this.checkPermission(subject);
        subject.setIsDeleted(1);
        subject.setUpdatedBy(HelperService.getCurrentUser().getId());
        subjectRepository.save(subject);
    }

    @Override
    public Subject create(SubjectDTO subjectDTO) {
        Subject subject = converterService.subjectDTOToSubject(subjectDTO);
        subject.setCreatedBy(HelperService.getCurrentUser().getId());
        subject.setUpdatedBy(HelperService.getCurrentUser().getId());
        return subjectRepository.save(subject);
    }

    @Override
    public Subject update(String id, SubjectDTO subject) {
        Subject oldSubject = this.findById(id);
        this.checkPermission(oldSubject);
        oldSubject.setDescription(subject.getDescription());
        oldSubject.setName(subject.getName());
        oldSubject.setCategory(subject.getCategory());
        oldSubject.setDescription(subject.getDescription());
        oldSubject.setUpdatedAt(Instant.now());
        oldSubject.setUpdatedBy(HelperService.getCurrentUser().getId());
        return subjectRepository.save(oldSubject);
    }

    @Override
    public void checkPermission(BaseModel baseModel) {
        UserDetailsImpl con = HelperService.getCurrentUser();

        boolean isAdmin = false;

        for(GrantedAuthority authority : con.getAuthorities()){
            if(authority.getAuthority().equals("ROLE_ADMIN")){
                isAdmin = true;
            }
        }

        if(!(con.getId().equals(((Subject)baseModel).getCreatedBy()) || isAdmin)){
            throw new RuntimeException("403");
        }
    }
}
