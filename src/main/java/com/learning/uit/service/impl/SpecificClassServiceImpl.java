package com.learning.uit.service.impl;

import com.learning.uit.auth.UserDetailsImpl;
import com.learning.uit.common.Constants;
import com.learning.uit.coverter.ConverterService;
import com.learning.uit.dto.ClassDTO;
import com.learning.uit.dto.TeachingDTO;
import com.learning.uit.helper.HelperService;
import com.learning.uit.model.*;
import com.learning.uit.model.base.BaseModel;
import com.learning.uit.repository.SpecificClassRepository;
import com.learning.uit.security.WebSecurityConfig;
import com.learning.uit.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import javax.ws.rs.ForbiddenException;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collection;
import java.util.stream.Collectors;

@Service
@Transactional
public class SpecificClassServiceImpl implements SpecificClassService, BaseService {

    @Autowired
    private SpecificClassRepository specificClassRepository;

    @Autowired
    private CourseService courseService;

    @Autowired
    private UserService userService;

    @Autowired
    private ScheduleService scheduleService;

    @Autowired
    private SessionService sessionService;

    @Autowired
    private StudyingService studyingService;

    @Autowired
    private TeachingService teachingService;

    @Autowired
    private ConverterService converterService;

    @Autowired
    private HelperService helperService;

    @Override
    public Collection<SpecificClass> findAll() {
        return specificClassRepository.findAll();
    }

    @Override
    public SpecificClass findById(String id) {
        return specificClassRepository.findById(id).orElseThrow(() -> new RuntimeException("no class has id: " + id));
    }

    @Override
    public Collection<SpecificClass> findByName(String name) {
        return specificClassRepository.findByName(name);
    }

    @Override
    public Collection<SpecificClass> findByStatus(int status) {
        return specificClassRepository.findByStatus(status);
    }

    @Override
    public Collection<SpecificClass> findByTutor(String tutorId) {
        Collection<SpecificClass> res = new ArrayList<>();
        for(Teaching teaching : userService.findById(tutorId).getTeachings()){
            res.add(teaching.getSpecificClass());
        }
        return res;
    }

    @Override
    public Collection<SpecificClass> findByStudent(String studentId) {
        Collection<SpecificClass> res = new ArrayList<>();
        for(Studying studying : userService.findById(studentId).getStudyings()){
            res.add(studying.getSpecificClass());
        }
        return res;
    }

    @Override
    public Collection<SpecificClass> findBySubject(String subjectId) {
        Collection<SpecificClass> res = new ArrayList<>();
        for(Course course : courseService.findBySubject(subjectId)){
            for(SpecificClass c : course.getSpecificClasses()){
                res.add(c);
            }
        }
        return res;
    }

    @Override
    public Collection<SpecificClass> findByCourse(String courseId) {
        Collection<SpecificClass> specificClasses = specificClassRepository.findByCourse(courseId).stream()
                .filter(specificClass -> specificClass.getCourse().getIsDeleted() == 0 && specificClass.getCourse().getSubject().getIsDeleted() ==0)
                .collect(Collectors.toList());
        return specificClasses;
    }

    @Override
    public Collection<SpecificClass> findManyByIds(Collection<String> ids) {
        Collection<SpecificClass> specificClasses = new ArrayList<>();
        for(String id : ids){
            specificClasses.add(this.findById(id));
        }
        return specificClasses;
    }

    @Override
    public long getClassDuration(String classId) {
        SpecificClass specificClass = this.findById(classId);

        long currentDuration = 0;

        for (Schedule schedule : specificClass.getSchedules()){
            currentDuration += schedule.getStartAt().until(schedule.getEndAt(), ChronoUnit.SECONDS);
        }

        return currentDuration;
    }

    @Override
    public SpecificClass deleteById(String specificClassId) {
        SpecificClass specificClass = this.findById(specificClassId);
        this.checkPermission(specificClass);
        specificClass.setIsDeleted(1);
        specificClass.setUpdatedAt(Instant.now());
        specificClass.setUpdatedBy(HelperService.getCurrentUser().getId());
        return specificClassRepository.save(specificClass);
    }

    @Override
    public SpecificClass updateById(String classId, ClassDTO classDTO) {
//        teachingService.delete(teachingService.findByClass(classId));

        SpecificClass specificClass = this.findById(classId);
        this.checkPermission(specificClass);
        specificClass.setClassName(classDTO.getClassName());
        specificClass.setUpdatedAt(Instant.now());
//        specificClass.setSchedules(scheduleService.findManyByIds(classDTO.getScheduleIds()));
        specificClass.setUpdatedBy(HelperService.getCurrentUser().getId());
        specificClass.setSyllabus(classDTO.getSyllabus());
        specificClass.setIsDeleted(classDTO.getIsDeleted());
        specificClass.setStatus(classDTO.getStatus());
        specificClass.setBegin(helperService.stringToInstant(classDTO.getBegin()));
        specificClass.setCourse(courseService.findById(classDTO.getCourseId()));
//        specificClass.setSessions(sessionService.findManyByIds(classDTO.getSessionIds()));
        specificClass.setDescription(classDTO.getDescription());
        specificClassRepository.save(specificClass);


//        TeachingDTO teachingDTO = classDTO.getTeachingDTO();
//        teachingDTO.setSpecificClassId(specificClass.getId());
//        teachingService.create(teachingDTO);

        return specificClass;
    }

    @Override
    public SpecificClass create(ClassDTO classDTO) {
        SpecificClass specificClass = converterService.classDTOToClass(classDTO);
        specificClass.setDescription(classDTO.getDescription());
        specificClass.setIsDeleted(classDTO.getIsDeleted());
        specificClass.setStatus(Constants.CLASS_READY);
        specificClass.setCreatedBy(HelperService.getCurrentUser().getId());
        specificClass.setEnd(HelperService.stringToInstant(classDTO.getEnd()));
        specificClassRepository.save(specificClass);
        TeachingDTO teachingDTO = classDTO.getTeachingDTO();
        teachingDTO.setSpecificClassId(specificClass.getId());
        teachingDTO.setCreatedBy(HelperService.getCurrentUser().getId());
        teachingService.create(teachingDTO);
        return specificClass;
    }

    @Override
    public void checkPermission(BaseModel baseModel) {
        UserDetailsImpl con = HelperService.getCurrentUser();

        boolean isAdmin = false;

        for(GrantedAuthority authority : con.getAuthorities()){
            if(authority.getAuthority().equals("ROLE_ADMIN")){
                isAdmin = true;
            }
        }

        String teacherId = userService.findTeacherByClass(((SpecificClass)baseModel).getId()).getId();

        if(!(con.getId().equals(teacherId) || isAdmin)){
            throw new RuntimeException("403");
        }
    }
}
