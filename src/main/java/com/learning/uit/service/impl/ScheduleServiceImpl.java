package com.learning.uit.service.impl;

import com.learning.uit.auth.UserDetailsImpl;
import com.learning.uit.common.Constants;
import com.learning.uit.coverter.ConverterService;
import com.learning.uit.dto.ScheduleDTO;
import com.learning.uit.exception.ScheduleConflictException;
import com.learning.uit.helper.HelperService;
import com.learning.uit.model.*;
import com.learning.uit.model.base.BaseModel;
import com.learning.uit.repository.ScheduleRepository;
import com.learning.uit.repository.SpecificClassRepository;
import com.learning.uit.response.UpdateEndDateResponse;
import com.learning.uit.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class ScheduleServiceImpl implements ScheduleService, BaseService {

    @Autowired
    private ScheduleRepository scheduleRepository;

    @Autowired
    private StudyingService studyingService;

    @Autowired
    private TeachingService teachingService;

    @Autowired
    private UserService userService;

    @Autowired
    private ConverterService converterService;

    @Autowired
    private CourseService courseService;

    @Autowired
    private HelperService helperService;

    @Autowired
    private SpecificClassService specificClassService;

    @Autowired
    private SpecificClassRepository specificClassRepository;

    @Override
    public Collection<Schedule> findAll() {
        return scheduleRepository.findAll();
    }

    @Override
    public Schedule findById(String id) {
        return scheduleRepository.findById(id).orElseThrow(() -> new RuntimeException("no schedule has id: " + id));
    }

    @Override
    public Collection<Schedule> findByClass(String classId) {
        return scheduleRepository.findByClass(classId);
    }

    @Override
    public Collection<Schedule> findByTutor(String id) {
        Collection<Schedule> res = new ArrayList<>();
        for(Teaching teaching : userService.findById(id).getTeachings()){
            for(Schedule schedule : teaching.getSpecificClass().getSchedules()){
                res.add(schedule);
            }
        }
        return res.size() == 0 ? null : res;
    }

    @Override
    public Collection<Schedule> findByStudent(String id) {
        Collection<Schedule> res = new ArrayList<>();
        for(Studying studying : userService.findById(id).getStudyings()){
            for(Schedule schedule : studying.getSpecificClass().getSchedules()){
                res.add(schedule);
            }
        }
        return res.size() == 0 ? null : res;
    }

    @Override
    public Collection<Schedule> findManyByIds(Collection<String> ids) {
        Collection<Schedule> schedules = null;
        for(String id : ids){
            schedules.add(this.findById(id));
        }
        return schedules;
    }

    @Override
    public Collection<Schedule> findByCourse(String courseId) {
        Collection<Schedule> schedules = this.findAll().stream().collect(Collectors.toList());
        Collection<Schedule> res = new ArrayList<>();
        Collection<SpecificClass> classes = specificClassService.findByCourse(courseId);
        if(classes == null || schedules == null){
            return null;
        }
        for(Schedule schedule : schedules){
            for (SpecificClass clazz : classes){
                if(schedule.getSepecificClass().getId().equals(clazz.getId())){
                    res.add(schedule);
                }
            }
        }
        return res;
    }

    @Override
    public Collection<Schedule> findByDayOfWeekAndClass(String classId, int dayOfWeek) {
        return this.findAll().stream()
                .filter(schedule -> schedule.getDay() == dayOfWeek && schedule.getSepecificClass().getId().equals(classId))
                .collect(Collectors.toList());
    }

    @Override
    public long getDurationSchedule(Schedule schedule) {
        return schedule.getStartAt().until(schedule.getEndAt(), ChronoUnit.SECONDS);
    }

    @Override
    public boolean deleteById(String scheduleId) {
        Schedule schedule = this.findById(scheduleId);
        this.checkPermission(schedule);
        schedule.setUpdatedBy(HelperService.getCurrentUser().getId());
        schedule.setUpdatedAt(Instant.now());
        schedule.setIsDeleted(1);
        UpdateEndDateResponse response = this.updateEndDate(schedule, Constants.DELETE);
        if(response.isReturning()){
            updateEndDateForClass(schedule.getSepecificClass().getId(), response.getEndDate());
            scheduleRepository.save(schedule);
        }
        scheduleRepository.save(schedule);
//        return response.isReturning();
        return true;
    }

    private void updateEndDateForClass(String byId, Instant endDate) {
        SpecificClass specificClass = specificClassService.findById(byId);
        specificClass.setEnd(endDate);
        specificClassRepository.save(specificClass);
    }

    @Override
    public Schedule create(ScheduleDTO scheduleDTO) {
        String userId = HelperService.getCurrentUser().getId();
        Collection<Teaching> teachings = teachingService.findByUser(userService.findById(userId));
        Collection<SpecificClass> classes = new ArrayList<>();

        for (Teaching teaching : teachings){
            if(teaching.getSpecificClass().getStatus().equals(Constants.CLASS_READY)){
                classes.add(teaching.getSpecificClass());
            }
        }

        Collection<Schedule> totalSchedule = new ArrayList<>();

        for (SpecificClass clazz : classes) {
            for (Schedule schedule : clazz.getSchedules()){
                totalSchedule.add(schedule);
            }
        }


        Schedule schedule = converterService.scheduleDTOToSchedule(scheduleDTO);

        for (Schedule sch : totalSchedule){
            if (this.checkDateConflict(sch, schedule)){
                throw new ScheduleConflictException("schedule conflicts");
            }
        }

        this.checkPermission(schedule);
        schedule.setIsDeleted(scheduleDTO.getIsDeleted());
        schedule.setCreatedBy(HelperService.getCurrentUser().getId());
        schedule.setUpdatedBy(HelperService.getCurrentUser().getId());
        schedule.setDescription(scheduleDTO.getDescription());


        Schedule result = null;
//        UpdateEndDateResponse end = this.updateEndDate(schedule, Constants.INSERT);
//
//        if(end.isReturning() == true){
//            result = scheduleRepository.save(schedule);
////            SpecificClass clazz = schedule.getSepecificClass();
//////            clazz.setEnd(end.getEndDate().toInstant(ZoneOffset.UTC));
////            clazz.setEnd(end.getEndDate());
////            specificClassRepository.save(clazz);
//            updateEndDateForClass(result.getSepecificClass().getId(), end.getEndDate());
//        }
        result = scheduleRepository.save(schedule);
        return result;
    }

    @Override
    public Schedule update(String scheduleId, ScheduleDTO scheduleDTO) {
        Schedule schedule = this.findById(scheduleId);
        this.checkPermission(schedule);
        schedule.setDescription(scheduleDTO.getDescription());
//        schedule.setCourse(courseService.findById(scheduleDTO.getCourseId()));
        schedule.setDay(scheduleDTO.getDay());
        schedule.setUpdatedAt(Instant.now());
        schedule.setUpdatedBy(HelperService.getCurrentUser().getId());
//        schedule.setSepecificClass(specificClassService.findById(scheduleDTO.getSepecificClassId()));
        schedule.setStartAt(LocalTime.parse(scheduleDTO.getStartAt()));
        schedule.setEndAt(LocalTime.parse(scheduleDTO.getEndAt()));
        schedule.setDescription(scheduleDTO.getDescription());

        UpdateEndDateResponse response = this.updateEndDate(schedule, Constants.UPDATE);

//        if(response.isReturning()){
//            updateEndDateForClass(schedule.getSepecificClass().getId(), response.getEndDate());
//            return scheduleRepository.save(schedule);
//        }else {
//            return null;
//        }
        return scheduleRepository.save(schedule);
    }

    @Override
    public boolean checkForLearning(Collection<Schedule> scheduleCollection, Schedule schedule){
        if(scheduleCollection == null){
            return true;
        }
        for(Schedule scheduleDetail : scheduleCollection){
                if(this.checkDateConflict(schedule, scheduleDetail)){
                    return false;
                }
        }
        return true;
    }

    @Override
    //false is not conflictful, true is conflictful
    public boolean checkDateConflict(Schedule s1, Schedule s2){
        if(s1 == null || s2 == null){
            return false;
        }
        if(s1.getDay() != s2.getDay()){
            return false;
        }
        boolean b1 = (s1.getStartAt().compareTo(s2.getStartAt()) <= 0 && s1.getEndAt().compareTo(s2.getStartAt()) >= 0)
                || (s1.getStartAt().compareTo(s2.getEndAt()) <= 0 && s1.getEndAt().compareTo(s2.getStartAt()) >= 0)
                ;
        boolean b2 = (s2.getStartAt().compareTo(s1.getStartAt()) <= 0 && s2.getEndAt().compareTo(s1.getStartAt()) >= 0)
                || (s2.getStartAt().compareTo(s1.getEndAt()) <= 0 && s2.getEndAt().compareTo(s1.getStartAt()) >= 0)
                ;

        return b1 || b2;
    }

    @Override
    public Collection<Schedule> createMany(Collection<ScheduleDTO> scheduleDTOS) {
        Collection<Schedule> res = new ArrayList<>();
        for(ScheduleDTO dto : scheduleDTOS){
            res.add(scheduleRepository.saveAndFlush(converterService.scheduleDTOToSchedule(dto)));
        }
        return res;
    }

    @Override
    public void checkPermission(BaseModel baseModel) {
        UserDetailsImpl con = HelperService.getCurrentUser();

        boolean isAdmin = false;

        for(GrantedAuthority authority : con.getAuthorities()){
            if(authority.getAuthority().equals("ROLE_ADMIN")){
                isAdmin = true;
            }
        }

        String classId = specificClassService.findById(((Schedule)baseModel)
                .getSepecificClass().getId()).getId();

        String uId = userService.findTeacherByClass(classId).getId();

        if(!(con.getId().equals(uId) || isAdmin)){
            throw new RuntimeException("403");
        }
    }

    @Override
    public UpdateEndDateResponse updateEndDate(Schedule schedule, String action) {
        SpecificClass specificClass = specificClassService.findById(schedule.getSepecificClass().getId());
        Course course = specificClass.getCourse();
        long courseDuration = course.getDuration();
        Collection<Schedule> availableSchedules = this.findByClass(specificClass.getId());

        switch (action){
            case Constants.INSERT:{
                availableSchedules.add(schedule);
                break;
            }
            case Constants.UPDATE:{
                for(Schedule sc : availableSchedules){
                    if(sc.getId().equals(schedule.getId())){
                        availableSchedules.remove(sc);
                        availableSchedules.add(schedule);
                    }
                }
                break;
            }
            case Constants.DELETE:{
                for (Schedule sc : availableSchedules){
                    if (sc.getId().equals(schedule.getId())){
                        availableSchedules.remove(sc);
                    }
                }
                break;
            }
        }

        Collection<Schedule> sortedSchedules = this.sortSchedulesByDay(availableSchedules);

        Schedule firstScheduleInWeek = sortedSchedules.stream().findFirst().orElse(null);

//        LocalDateTime beginDate = LocalDateTime.ofInstant(specificClass.getBegin(),ZoneOffset.UTC);
        Instant beginDate = specificClass.getBegin();

        Schedule[] timeTable = availableSchedules.toArray(new Schedule[availableSchedules.size()]);
        int min = 8;
        int minIndex =0;
        int loop = 0;

        for(int i = 0; i < timeTable.length;i++){
            int sub = timeTable[i].getDay() - firstScheduleInWeek.getDay();
            if(sub < min && sub >= 0){
                min = sub;
                minIndex = i;
            }
        }

        for (int i = minIndex; i < timeTable.length; i++){
            long duration = this.getDurationSchedule(timeTable[i]);
            if (duration > courseDuration){
                break;
            }
            if(courseDuration - duration < 0){
                break;
            }
            if(i < timeTable.length - 1){
                if(courseDuration - duration >= 0) {
                    if (timeTable[i].getDay() != timeTable[i + 1].getDay()){
                        Calendar preCal = Calendar.getInstance();
//                        preCal.setTime(Date.from(beginDate.toInstant(ZoneOffset.UTC)));
                        preCal.setTime(Date.from(beginDate));
                        int pre = preCal.get(Calendar.DAY_OF_WEEK);

                        while (pre != timeTable[i + 1].getDay()){
//                            beginDate.plusDays(1);
                            beginDate = beginDate.plus(1, ChronoUnit.DAYS);
//                            preCal.setTime(Date.from(beginDate.toInstant(ZoneOffset.UTC)));
                            preCal.setTime(Date.from(beginDate));
                            pre = preCal.get(Calendar.DAY_OF_WEEK);
                        }
                    }else {
                        if(courseDuration - duration >= 0){
//                            beginDate.plusDays(1);
                            beginDate = beginDate.plus(1, ChronoUnit.DAYS);
                        }
                    }
                }
            }
            courseDuration -= duration;
            System.out.println("loop "+loop+++":"+courseDuration+":"+beginDate);
        }
        boolean br = false;
        do{
            for (int i = 0; i < timeTable.length; i++){
                long duration = this.getDurationSchedule(timeTable[i]);
                if (duration > courseDuration){
                    br = true;
                    break;
                }
                if(courseDuration - duration < 0){
                    br = true;
                    break;
                }
                if(i < timeTable.length - 1){
                    if(courseDuration - duration >= 0
                            && timeTable[i].getDay() != timeTable[i + 1].getDay()) {

                        Calendar preCal = Calendar.getInstance();
//                        preCal.setTime(Date.from(beginDate.toInstant(ZoneOffset.UTC)));
                        preCal.setTime(Date.from(beginDate));
                        int pre = preCal.get(Calendar.DAY_OF_WEEK);

                        while (pre != timeTable[i + 1].getDay()){
//                            beginDate.plusDays(1);
//                            preCal.setTime(Date.from(beginDate.toInstant(ZoneOffset.UTC)));
                            beginDate = beginDate.plus(1, ChronoUnit.DAYS);
                            preCal.setTime(Date.from(beginDate));
                            pre = preCal.get(Calendar.DAY_OF_WEEK);
                        }
                    }
                }else {
                    if(courseDuration - duration >= 0) {
//                        beginDate.plusDays(1);
                        beginDate = beginDate.plus(1, ChronoUnit.DAYS);
                    }
                }
                courseDuration -= duration;
                System.out.println("loop "+loop+++":"+courseDuration+":"+beginDate);
            }
            if (br){
                break;
            }
        }while (courseDuration >= 0);

        if(courseDuration != 0){
            return new UpdateEndDateResponse(false);
        }else {
            return new UpdateEndDateResponse(true, beginDate);
        }
    }

    public Collection<Schedule> sortSchedulesByDay(Collection<Schedule> schedules){
        List<Schedule> list = schedules.stream().collect(Collectors.toList());
        for(int i = 0; i<schedules.size() -1;i++){
            for (int j = i + 1; j<schedules.size();j++){
                if(list.get(i).getDay() > list.get(j).getDay()){
                    Schedule temp = list.get(i);
                    list.set(i, list.get(j));
                    list.set(j, temp);
                }
            }
        }
        return schedules;
    }
}
