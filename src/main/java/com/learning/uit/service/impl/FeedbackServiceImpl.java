package com.learning.uit.service.impl;

import com.learning.uit.auth.UserDetailsImpl;
import com.learning.uit.coverter.ConverterService;
import com.learning.uit.dto.FeedbackDTO;
import com.learning.uit.helper.HelperService;
import com.learning.uit.model.*;
import com.learning.uit.model.base.BaseModel;
import com.learning.uit.repository.FeedbackRepository;
import com.learning.uit.security.WebSecurityConfig;
import com.learning.uit.service.BaseService;
import com.learning.uit.service.FeedbackService;
import com.learning.uit.service.StudyingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import javax.ws.rs.ForbiddenException;
import java.time.Instant;
import java.util.Collection;

@Service
@Transactional
public class FeedbackServiceImpl implements FeedbackService, BaseService {

    @Autowired
    private FeedbackRepository feedbackRepository;

    @Autowired
    private ConverterService converterService;

    @Autowired
    private StudyingService studyingService;

    @Autowired
    private HelperService helperService;

    @Override
    public Feedback findById(String id) {
        return feedbackRepository.findById(id).orElseThrow(() -> new RuntimeException("no feedback has id: "+id));
    }

    @Override
    public Collection<Feedback> findAll() {
        return feedbackRepository.findAll();
    }

    @Override
    public Collection<Feedback> findByUser(String userId) {
        return feedbackRepository.findByUser(userId);
    }

    @Override
    public Collection<Feedback> findByClass(String classId) {
        return feedbackRepository.findByClass(classId);
    }

    @Override
    public Collection<Feedback> findByCourse(String courseId) {
        return feedbackRepository.findByCourse(courseId);
    }

    @Override
    public void deleteById(String feedbackId) {

    }

    @Override
    public Feedback create(FeedbackDTO feedbackDTO) {
        Feedback feedback = converterService.feedbackDTOToFeedback(feedbackDTO);
        feedback.setCreatedBy(HelperService.getCurrentUser().getId());
        Studying studying = feedback.getStudying();
        Collection<Feedback> feedbacks = feedbackRepository.findByUser(HelperService.getCurrentUser().getId());
        for(Feedback fb : feedbacks){
            if(fb.getId().equals(studying.getUser().getId())){
                throw new RuntimeException("You have only 1 feedback for this class");
            }
        }
        this.checkPermission(feedback);
        return feedbackRepository.save(feedback);
    }

    @Override
    public Feedback update(String feedbackId, FeedbackDTO feedbackDTO) {
        Feedback feedback = this.findById(feedbackId);
        this.checkPermission(feedback);
        feedback.setContent(feedbackDTO.getContent());
        feedback.setRate(feedbackDTO.getRate());
        feedback.setDescription(feedbackDTO.getDescription());
        feedback.setUpdatedAt(Instant.now());
        feedback.setUpdatedBy(HelperService.getCurrentUser().getId());
//        feedback.setUpdatedBy(feedbackDTO.getUpdatedBy());
        feedback.setUpdatedAt(Instant.now());
        feedbackRepository.save(feedback);
        return feedback;
    }

    @Override
    public void checkPermission(BaseModel baseModel) {
        UserDetailsImpl con = HelperService.getCurrentUser();

        boolean isAdmin = false;

        for(GrantedAuthority authority : con.getAuthorities()){
            if(authority.getAuthority().equals("ROLE_ADMIN")){
                isAdmin = true;
            }
        }

        Studying studying = ((Feedback)baseModel).getStudying();

        if(!(con.getId().equals(studying.getUser().getId()) || isAdmin)){
            throw new ForbiddenException();
        }
    }
}
