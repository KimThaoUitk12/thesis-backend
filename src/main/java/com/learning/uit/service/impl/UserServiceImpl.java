package com.learning.uit.service.impl;

import com.learning.uit.auth.UserDetailsImpl;
import com.learning.uit.coverter.ConverterService;
import com.learning.uit.dto.Account;
import com.learning.uit.dto.UpdateUserDTO;
import com.learning.uit.dto.UserDTO;
import com.learning.uit.dto.base.ExperienceDTO;
import com.learning.uit.helper.HelperService;
import com.learning.uit.model.*;
import com.learning.uit.model.base.BaseModel;
import com.learning.uit.model.base.Experience;
import com.learning.uit.repository.UserRepository;
import com.learning.uit.security.WebSecurityConfig;
import com.learning.uit.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import javax.ws.rs.ForbiddenException;
import java.time.Instant;
import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@Transactional
public class UserServiceImpl implements UserService, BaseService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ExperienceService experienceService;

    @Autowired
    private ConverterService converterService;

    @Autowired
    private StudyingService studyingService;

    @Autowired
    private TeachingService teachingService;

    @Autowired
    private HelperService helperService;

    @Autowired
    PasswordEncoder encoder;

    @Override
    public Collection<User> findAllByUser() {
        return userRepository.findAllByUser();
    }



    @Override
    public Collection<User> findByName(String name) {
        return userRepository.findByName(name);
    }

    @Override
    public User findByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    @Override
    public Collection<User> findAll() {
        return userRepository.findAll();
    }



    @Override
    public Collection<User> findLearnerByCourse(String idCourse) {
        return null;
    }

    @Override
    public Collection<User> findLearnerPaidCourse(String idCourse) {
        return null;
    }

    @Override
    public Collection<User> findLearnerDidntPaidCourse(String idCourse) {
        return null;
    }

    @Override
    public User findLearnerByClass(String classId) {
        User user = null;
        for(Studying studying : studyingService.findAll()){
            if(studying.getSpecificClass().getId().equals(classId)){
                user = this.findById(studying.getUser().getId());
            }
        }
        return user;
    }

    @Override
    public User findTeacherByClass(String classId) {
        Teaching t = null;
        for(Teaching teaching : teachingService.findAll()){
            SpecificClass clazz = teaching.getSpecificClass();
            if(clazz.getId().equals(classId)){
                t = teaching;
            }
        }
        return this.findById(t.getUser().getId());
    }

    @Override
    public User deleteById(String id) {
        User user = this.findById(id);
        user.setIsDeleted(1);
        user.setUpdatedAt(Instant.now());
        user.setUpdatedBy(HelperService.getCurrentUser().getId());
        return userRepository.save(user);
    }

    @Override
    public User findById(String id) {
        return userRepository.findById(id).orElseThrow(()-> new RuntimeException("no user has id: " + id));
    }

    @Override
    @Transactional
    public void deleteMany(Collection<String> ids) {
        ids.stream().map(id -> this.deleteById(id));
    }

    @Override
    @Transactional
    public User updateByUser(String userId, UpdateUserDTO userData) {
        User user = this.findById(userId);
        user.setAddress(userData.getAddress());
        user.setAvatar(userData.getAvatar());
        user.setFirstName(userData.getFirstName());
        user.setLastName(userData.getLastName());
        user.setPhone(userData.getPhone());
        user.setUpdatedAt(Instant.now());
        user.setDescription(userData.getDescription());
        user.setUpdatedBy(HelperService.getCurrentUser().getId());
        return userRepository.save(user);
    }

    @Override
    @Transactional
    public User updateByAdmin(String userId, UserDTO userDTo) {
        User user = this.findById(userId);
        this.checkPermission(user);
        user.setPhone(userDTo.getPhone());
        user.setAddress(userDTo.getAddress());
        user.setEnabled(userDTo.getEnabled());
        user.setAvatar(userDTo.getAvatar());
        user.setDescription(userDTo.getDescription());
        user.setUpdatedAt(Instant.now());
        user.setUpdatedBy(HelperService.getCurrentUser().getId());
        return userRepository.save(user);
    }

    @Override
    @Transactional
    public void updateAccount(String idUser, Account account) {
        User user = this.findById(idUser);
        this.checkPermission(user);
        user.setPassword(encoder.encode(account.getPassword()));
        user.setUpdatedAt(Instant.now());
        user.setUpdatedBy(HelperService.getCurrentUser().getId());
        userRepository.save(user);
    }

    @Override
    @Transactional
    public User createUser(UserDTO userDTO) {
        User user = User.builder()
                .address(userDTO.getAddress())
                .firstName(userDTO.getFirstName())
                .lastName(userDTO.getLastName())
                .email(userDTO.getEmail())
                .password(userDTO.getPassword())
                .avatar(userDTO.getAvatar())
                .address(userDTO.getAddress())
                .phone(userDTO.getPhone())
                .build();
        Set<Role> roleSet = userDTO.getRoles().stream().map(role -> converterService.stringToRole(role)).collect(Collectors.toSet());
        user = userRepository.save(user);
        if(userDTO.getExperienceDTOS() != null){
            for (ExperienceDTO dto : userDTO.getExperienceDTOS()){
                dto.setUserId(user.getId());
            }
            Collection<Experience> experiences = userDTO.getExperienceDTOS().stream()
                    .map(experienceDTO -> converterService.experienceDTOToExperience(experienceDTO))
                    .collect(Collectors.toList());
//        user = userRepository.save(user);
            for(Experience experience : experiences){
                experience.setUser(user);
            }
            user.setExperiences(experiences);
        }
        user.setRoles(roleSet);
        user.setCreatedBy(user.getId());
        user.setUpdatedBy(user.getId());

        return userRepository.saveAndFlush(user);
    }

    @Override
    public void checkPermission(BaseModel baseModel) {
        UserDetailsImpl con = HelperService.getCurrentUser();

        boolean isAdmin = false;

        for(GrantedAuthority authority : con.getAuthorities()){
            if(authority.getAuthority().equals("ROLE_ADMIN")){
                isAdmin = true;
            }
        }

        if(!(con.getId().equals(((User)baseModel).getId()) || isAdmin)){
            throw new ForbiddenException();
        }
    }
}
