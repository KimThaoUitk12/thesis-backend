package com.learning.uit.service;

import com.learning.uit.dto.TeachingDTO;
import com.learning.uit.model.Teaching;
import com.learning.uit.model.User;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public interface TeachingService {
    Collection<Teaching> findAll();
    Teaching findById(String id);
    Collection<Teaching> findByUser(User user);
    Teaching findByClass(String classId);
    Collection<Teaching> findReceivedByUser(int receipt, String teacherId);
    Teaching create(TeachingDTO teachingDTO);
    Teaching update(String classId, TeachingDTO teachingDTO);
    void delete(Teaching teaching);
}
