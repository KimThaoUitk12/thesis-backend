package com.learning.uit.service;

import com.learning.uit.dto.FeedbackDTO;
import com.learning.uit.model.Feedback;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public interface FeedbackService {
    Feedback findById(String id);
    Collection<Feedback> findAll();
    Collection<Feedback> findByUser(String userId);
    Collection<Feedback> findByClass(String classId);
    Collection<Feedback> findByCourse(String courseId);

    void deleteById(String feedbackId);
    Feedback create(FeedbackDTO feedback);
    Feedback update(String feedbackId, FeedbackDTO feedbackDTO);
}
