package com.learning.uit.common;

public enum ERole {
    ROLE_USER,
    ROLE_ADMIN
}
