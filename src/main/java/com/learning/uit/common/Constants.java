package com.learning.uit.common;

public class Constants {
    public static final String CLASS_PROCESS  = "process";
    public static final String CLASS_READY = "ready";
    public static final String CLASS_CLOSED = "closed";

    public static final int MONDAY = 2;
    public static final int TUESDAY = 3;
    public static final int WEDNESDAY = 4;
    public static final int THURSDAY = 5;
    public static final int FRIDAY = 6;
    public static final int SATURDAY = 7;
    public static final int SUNDAY = 1;

    public static final String INSERT = "insert";
    public static final String UPDATE = "update";
    public static final String DELETE = "delete";
}
