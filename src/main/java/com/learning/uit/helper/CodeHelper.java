package com.learning.uit.helper;

import java.util.Random;

public class CodeHelper {

    public static Random ran = new Random();

    public static String generateCode(String name){
        return name + Integer.toString(ran.nextInt(100000));
    }

    private static String handleIncrement(long increment) {
        String res = "";
        double temp = (double) increment/1000000;
        for(char c : String.valueOf(temp).toCharArray()){
            if(c == '.'){
                break;
            }
            res += c;
        }

        return res;
    }
}
