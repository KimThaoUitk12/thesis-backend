package com.learning.uit.helper;

import com.learning.uit.config.FileStorageProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

@Service
public class FileStorageServiceImpl implements FileStorageService {

    private Path fileStorageLocation;

    @Autowired
    public  FileStorageServiceImpl(FileStorageProperties fileStorageProperties) throws IOException {
        this.fileStorageLocation = Paths.get(fileStorageProperties.getUploadDir())
                .toAbsolutePath().normalize();

        try {
            Files.createDirectories(this.fileStorageLocation);
        }catch (Exception exception){
            throw new IOException("Could not create the directory where the uploaded files will be stored. ", exception);
        }
    }

    @Override
    public String storeFile(MultipartFile file) throws IOException {
        //Normalize file name
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());
        try{
            if (fileName.contains("..")){
                throw new IOException("sorry! Filename contains invalid path sequence " + fileName);
            }

            // Copy file to the target location (Replacing existing file with the same name)
            Path targetLocation = this.fileStorageLocation.resolve(fileName);
            Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);

            return fileName;
        }catch (IOException exception){
            exception.printStackTrace();
            throw new IOException("Could not store file " + fileName + ". Please try again!", exception);
        }
    }

    @Override
    public Resource loadFileAsResource(String fileName) throws IOException {
        try{
            Path filePath = this.fileStorageLocation.resolve(fileName).normalize();
            Resource resource = new UrlResource(filePath.toUri());
            if (resource.exists()){
                return resource;
            }else {
                throw new IOException("File not found " + fileName);
            }
        }catch (MalformedURLException exception){
            throw new FileNotFoundException("File not found " + fileName);
        }
    }
}
