package com.learning.uit.helper;

import com.learning.uit.auth.UserDetailsImpl;
import com.learning.uit.dto.ClassDTO;
import com.learning.uit.dto.ScheduleDTO;
import com.learning.uit.model.*;
import com.learning.uit.response.ClassResponse;
import com.learning.uit.response.CourseResponse;
import com.learning.uit.response.FeedbackResponse;
import com.learning.uit.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.IllegalFormatException;
import java.util.Locale;

@Component
public class HelperService {

    @Autowired
    private CourseService courseService;

    @Autowired
    private UserService userService;

    @Autowired
    private ScheduleService scheduleService;

    @Autowired
    SubjectService subjectService;

    @Autowired
    private FeedbackService feedbackService;

    @Autowired
    private StudyingService studyingService;

    public static UserDetailsImpl getCurrentUser(){
        return (UserDetailsImpl) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }

    public static Instant stringToInstant(String time){
        //"04:30 PM, Sat 5/12/2018"
        return LocalDateTime.parse(                   // Parse as an indeterminate `LocalDate`, devoid of time zone or offset-from-UTC. NOT a moment, NOT a point on the timeline.
                time ,        // This input uses a poor choice of format. Whenever possible, use standard ISO 8601 formats when exchanging date-time values as text. Conveniently, the java.time classes use the standard formats by default when parsing/generating strings.
                DateTimeFormatter.ofPattern( "yyyy-MM-dd HH:mm:ss")  // Use single-character `M` & `d` when the number lacks a leading padded zero for single-digit values.
        )                                      // Returns a `LocalDateTime` object.
                .atZone(                               // Apply a zone to that unzoned `LocalDateTime`, giving it meaning, determining a point on the timeline.
                        ZoneId.of( "Asia/Ho_Chi_Minh" )     // Always specify a proper time zone with `Contintent/Region` format, never a 3-4 letter pseudo-zone such as `PST`, `CST`, or `IST`.
                )                                      // Returns a `ZonedDateTime`. `toString` → 2018-05-12T16:30-04:00[America/Toronto].
                .toInstant()                           // Extract a `Instant` object, always in UTC by definition.
                ;
    }

    public boolean checkDurationFitsCourse(ClassDTO classDTO){
        Course course = courseService.findById(classDTO.getCourseId());
        Collection<ScheduleDTO> scheduleDTOS = classDTO.getScheduleDTOS();
        long sum = 0;
        for (ScheduleDTO scheduleDTO : scheduleDTOS){
            sum += this.durationForASchedule(scheduleDTO);
        }
        return course.getDuration() == sum;
    }

    public long stringToLocalTimeToSecond(String s){
        try {
            return LocalTime.parse(s, DateTimeFormatter.ofPattern("HH:mm:[ss]")).toSecondOfDay();
        }catch (Exception exception){
            exception.printStackTrace();
        }
        return 0;
    }

    public LocalTime stringToLocalTime(String s){
        try {
            return LocalTime.parse(s, DateTimeFormatter.ofPattern("HH:mm:[ss]"));
        }catch (Exception exception){
            exception.printStackTrace();
        }
        return null;
    }

    public long durationForASchedule(ScheduleDTO scheduleDTO){
        return this.stringToLocalTimeToSecond(scheduleDTO.getEndAt())
                - this.stringToLocalTimeToSecond(scheduleDTO.getStartAt());
    }

    public ClassResponse specificClassToClassResponse(SpecificClass specificClass){
        Collection<Feedback> feedbacks = null;
        Subject subject = null;
        Course course = null;
        Studying studying = null;
        subject = subjectService.findByClass(specificClass.getId());
        feedbacks = feedbackService.findByClass(specificClass.getId());
        course = courseService.findByClass(specificClass.getId());
        for(Studying st : studyingService.findAll()){
            if(st.getSpecificClass().getId().equals(specificClass.getId())){
                studying = st;
            }
        }
        return new ClassResponse(specificClass, userService.findTeacherByClass(specificClass.getId()),
                userService.findLearnerByClass(specificClass.getId()), course,
                subject, feedbacks, studying);
    }
    public CourseResponse courseToCourseResponse(Course course){
        Subject subject = null;
        subject = subjectService.findById(course.getSubject().getId());
        Collection<Feedback> feedbacks = feedbackService.findByCourse(course.getId());
        User teacher = course.getUser();
        Collection<User> students = userService.findLearnerByCourse(course.getId());
        return new CourseResponse(course, subject, courseService.getRateByCourse(course.getId()), feedbacks, teacher, students);
    }
    public FeedbackResponse feedbackToFeedbackResponse(Feedback feedback){
        return new FeedbackResponse(feedback, userService.findLearnerByClass(feedback.getStudying().getSpecificClass().getId()), feedback.getStudying().getSpecificClass());
    }
}
