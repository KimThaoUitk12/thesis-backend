package com.learning.uit.helper;

import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Service
public interface FileStorageService {
    String storeFile(MultipartFile file) throws IOException;
    Resource loadFileAsResource(String path) throws IOException;
}
