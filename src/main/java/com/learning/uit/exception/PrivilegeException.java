package com.learning.uit.exception;

public class PrivilegeException extends RuntimeException{
    public PrivilegeException(String message) {
        super(message);
    }
}
