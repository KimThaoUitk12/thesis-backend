package com.learning.uit.exception;

public class ScheduleConflictException extends RuntimeException{
    public ScheduleConflictException(String stackTrace){
        super(stackTrace);
    }
}
