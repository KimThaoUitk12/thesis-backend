package com.learning.uit.exception;

public class SubcribeException extends RuntimeException{
    public SubcribeException(String message) {
        super(message);
    }
}
