package com.learning.uit.exception;

public class DurationException extends Exception{
    public DurationException(String message) {
        super(message);
    }
}
